package com.io.pnbschool

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit esp_approach, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
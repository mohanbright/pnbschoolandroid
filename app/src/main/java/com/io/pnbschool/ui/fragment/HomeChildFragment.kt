package com.io.pnbschool.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.io.pnbschool.R
import com.io.pnbschool.base.BaseActivity
import com.io.pnbschool.base.BaseFragment
import com.io.pnbschool.base.BaseVM
import com.io.pnbschool.databinding.FragmentChildBinding
import com.io.pnbschool.ui.adapter.ImagesInfoAdapter
import com.io.pnbschool.ui.adapter.MyAdapter
import com.io.pnbschool.ui.home.MainActivity
import com.io.pnbschool.ui.video.VideoPlayerActivity
import com.io.pnbschool.utils.AppConstants


class HomeChildFragment : BaseFragment<FragmentChildBinding>() {
    override val layoutRes: Int
        get() = R.layout.fragment_child

    private lateinit var pagerAdapter: MyAdapter
    private lateinit var pagerFragment: PagerFragment
    private lateinit var mAdapter: ImagesInfoAdapter
    lateinit var homeBundle: Bundle
    var procedureFragment: ProcedureFragment = ProcedureFragment()


    /**
     * Create instance of Child
     */
    fun newInstance(args: Bundle): Fragment {
        val frag = HomeChildFragment()
        val bundle = Bundle()
        val s = args.getString(AppConstants.SEND_STRING)
        bundle.putString(AppConstants.SEND_STRING, s)
        bundle.putString(AppConstants.TOOLBAR_TITLE, args.getString(AppConstants.TOOLBAR_TITLE))
        frag.arguments = bundle
        return frag
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (requireActivity() as BaseActivity).getViewModel(BaseVM::class.java)

        _initialChild()

    }

    /**
     * Initial Child
     */
    @SuppressLint("UseRequireInsteadOfGet")
    fun _initialChild() {
        homeBundle = arguments!!
//        var a = AppConstants.orerViewHashMap()

        binding.s = homeBundle.getString(AppConstants.TOOLBAR_TITLE)

        binding.ivBack.setOnClickListener {
            if(requireActivity() is MainActivity){
                (requireActivity() as MainActivity).showNav()
            }
            val manager = requireActivity().supportFragmentManager
            manager.popBackStackImmediate()
            viewModel.observer.extraOpenfragment = viewModel.observer.extraOpenfragment - 1

        }

        fragment()
    }

    /**
     * Stup tab icon
     */
    private fun setupTabIcons() {
        binding.tabLayout.getTabAt(0)!!.text = "Overview"
        binding.tabLayout.getTabAt(1)!!.text = "Technique"
        binding.tabLayout.getTabAt(2)!!.text = "Tips"
    }


    /**
     * Fragment loader
     */
    @Synchronized
    private fun fragment() {
        pagerFragment = PagerFragment()
        pagerAdapter = MyAdapter(childFragmentManager)
        var bundle: Bundle
        for (i in 0..2) {
            when (i) {
                0 -> {
                    bundle = Bundle()
                    bundle.putString(
                        AppConstants.SEND_STRING,
                        homeBundle.getString(AppConstants.SEND_STRING)
                    )

                    bundle.putString(AppConstants.TAB_TYPE, "Overview")
                    pagerAdapter.addFragment(pagerFragment.newInstance(bundle))
                }
                1 -> {
                    bundle = Bundle()
                    bundle.putString(
                        AppConstants.SEND_STRING,
                        homeBundle.getString(AppConstants.SEND_STRING)
                    )
                    bundle.putString(AppConstants.TAB_TYPE, "Technique")
                    pagerAdapter.addFragment(pagerFragment.newInstance(bundle))
                }
                2 -> {
                    bundle = Bundle()
                    bundle.putString(
                        AppConstants.SEND_STRING,
                        homeBundle.getString(AppConstants.SEND_STRING)
                    )
                    bundle.putString(AppConstants.TAB_TYPE, "Tips")
                    pagerAdapter.addFragment(pagerFragment.newInstance(bundle))

                }
            }
        }

        binding.pager.adapter = pagerAdapter
        binding.tabLayout.setupWithViewPager(binding.pager)
        setupTabIcons()
        setUpAdapter()

    }

    /**
     * Setup adapter
     */
    private fun setUpAdapter() {
        mAdapter =
            ImagesInfoAdapter(requireContext(), homeBundle.getString(AppConstants.SEND_STRING)!!)
        binding.apply {
            rvChild.layoutManager = GridLayoutManager(activity, 2)
            adapter = mAdapter
        }
        showImagesandVideo(homeBundle.getString(AppConstants.SEND_STRING)!!)
    }


    @Synchronized
    fun showImagesandVideo(title: String) {
        // If url is from raw

        var anatomyButtonText = "Block Anatomy"
        val blockButtonText = "Block Procedure"
        var videoImage = 0
        when (title) {
            "Adductor Canal" -> {

                videoImage = R.drawable.adductor1
            }
            "ESP" -> {
                videoImage = R.drawable.esp_approach

            }
            "Femoral" -> {
                videoImage = R.drawable.femoral11
            }
            "FICB" -> {
                videoImage = R.drawable.fascialiaca7

            }
            "Interscalene" -> {

                videoImage = R.drawable.interscaleneapproach
            }
            "IPACK" -> {
                videoImage = R.drawable.ipackapproach

            }

            "Popliteal Sciatic" -> {
                anatomyButtonText = "Popliteal Sciatic"
                videoImage = R.drawable.sciaticapproach

            }
            "QL" -> {
                videoImage = R.drawable.anteriorqlapproach

            }
            "Rectus" -> {
                videoImage = R.drawable.rectus2
                binding.ivVideo.scaleType= ImageView.ScaleType.CENTER_CROP

            }
            "Supraclavicular" -> {
                videoImage = R.drawable.supraclavicularapproach

            }
            "Tap" -> {
                videoImage = R.drawable.tap3
            }
        }
        binding.apply {
            if (anatomyButtonText == "Popliteal Sciatic") {
                btnProcedure.isGone = true
            } else {
                btnProcedure.isVisible = true
            }
//            btnAnatomy.text = anatomyButtonText
            btnAnatomy.text = homeBundle.getString(AppConstants.TOOLBAR_TITLE)
            btnProcedure.text = blockButtonText

            ivVideo.setImageResource(videoImage)

            btnAnatomy.setOnClickListener {
                val extras = Bundle()
                extras.putSerializable(
                    "HashMap",
                    AppConstants.AnatomyHashmap(homeBundle.getString(AppConstants.SEND_STRING)!!)
                )
                loadNextFragment(R.id.src_container, procedureFragment.newInstance(extras))
            }
            btnProcedure.setOnClickListener {
                val extras = Bundle()
                extras.putSerializable(
                    "HashMap",
                    AppConstants.procedureHashmap(homeBundle.getString(AppConstants.SEND_STRING)!!)
                )
                loadNextFragment(R.id.src_container, procedureFragment.newInstance(extras))
            }

            ivVideo.setOnClickListener {
                startActivityWithPutExtra(
                    VideoPlayerActivity::class.java,
                    homeBundle.getString(AppConstants.SEND_STRING)!!
                )
            }

            ivPlay.setOnClickListener {
                startActivityWithPutExtra(
                    VideoPlayerActivity::class.java,
                    homeBundle.getString(AppConstants.SEND_STRING)!!
                )
            }
        }
    }

}

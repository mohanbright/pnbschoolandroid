package com.io.pnbschool.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.io.pnbschool.R
import com.io.pnbschool.base.BaseFragment
import com.io.pnbschool.databinding.FragmentPagerBinding
import com.io.pnbschool.utils.AppConstants
import com.io.pnbschool.utils.AppConstants.orerViewHashMap
import com.io.pnbschool.utils.AppConstants.techHashMap
import com.io.pnbschool.utils.AppConstants.tipsHashMap

class PagerFragment : BaseFragment<FragmentPagerBinding>() {

    override val layoutRes: Int
        get() = R.layout.fragment_pager

    lateinit var mBundle: Bundle
    var itemName = ""
    var mtabType = ""


    fun newInstance(args: Bundle): Fragment {
        val frag = PagerFragment()
        frag.arguments = args
        return frag

    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mBundle = arguments!!
        itemName = mBundle.getString(AppConstants.SEND_STRING)!!
        mtabType = mBundle.getString(AppConstants.TAB_TYPE)!!
        super.onViewCreated(view, savedInstanceState)
        when (mtabType) {
            "Overview" -> {
                binding.adapter = ItemAdapter(orerViewHashMap()[itemName]!!)

            }
            "Technique" -> {
                binding.adapter = ItemAdapter(techHashMap()[itemName]!!)
            }
            "Tips" -> {
                binding.adapter = ItemAdapter(tipsHashMap()[itemName]!!)
            }
        }

    }


    inner class ItemAdapter(var itemList: List<String>) :
        RecyclerView.Adapter<ItemAdapter.MyHolder>() {

        inner class MyHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            fun bind(item: String) {
                val tvItem = itemView.findViewById<TextView>(R.id.tv_text)
//                var ivCircle = itemView.findViewById<ImageView>(R.id.iv_circle)
                tvItem.text = item
//                if (tabType.equals("Technique")) {
//                    ivCircle.isGone = true
//                } else {
//                    ivCircle.isVisible = true
//                }
            }

        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapter.MyHolder {
            val view =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.pager_item_value, parent, false)
            return MyHolder(view)

        }

        override fun getItemCount(): Int = itemList.size


        override fun onBindViewHolder(holder: ItemAdapter.MyHolder, position: Int) {
            holder.apply {
                bind(itemList[position])
//                when (tabType) {
//                    "Overview" -> {
//                        bind(orerViewHashMap()[itemName]!![position])
//                    }
//                    "Technique" -> {
//                        bind(techHashMap()[itemName]!![position])
//                    }
//                    "Tips" -> {
//                        bind(tipsHashMap()[itemName]!![position])
//                    }
//
//                }
            }

        }
    }
}
package com.io.pnbschool.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.io.pnbschool.BuildConfig
import com.io.pnbschool.R

class SlidingImageAdapter(var context: Context, var images: List<Int>) : PagerAdapter() {

    private var inflater: LayoutInflater = LayoutInflater.from(context)


    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = images.size

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.slidingimages_layout, container, false)
        if (BuildConfig.DEBUG && imageLayout == null) {
            error("Assertion failed")
        }
        val imageView = imageLayout.findViewById<ImageView>(R.id.sliding_image)
        imageView.setImageResource(images[position])
        container.addView(imageLayout, 0)

        return imageLayout
    }

}

package com.io.pnbschool.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.io.pnbschool.R
import com.io.pnbschool.application.SchoolApp
import com.io.pnbschool.databinding.NavItemBinding

class NavigationParentAdapter(var context: Context, var drawerType: String) :
    RecyclerView.Adapter<NavigationParentAdapter.NavigationParentViewHolder>() {
    /**
     * ViewHolder of adapter
     */
    class NavigationParentViewHolder(var binding: NavItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): NavigationParentViewHolder {
        val binding = DataBindingUtil.inflate<NavItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.nav_item,
            parent,
            false
        )
        return NavigationParentViewHolder(binding)
    }

    override fun getItemCount(): Int {
        when (drawerType) {
            context.resources.getString(R.string.surgary) -> {
                return SchoolApp.abdominalTitleList().size
            }
            context.resources.getString(R.string.knee_artho) -> {
                return SchoolApp.totalKneeList().size
            }
            context.resources.getString(R.string.hip_artho) -> {
                return SchoolApp.totalHipList().size
            }
        }
        return 0
    }

    override fun onBindViewHolder(holder: NavigationParentViewHolder, position: Int) {
        holder.binding.apply {
            when (drawerType) {
                context.resources.getString(R.string.surgary) -> {
                    tvItem.text = SchoolApp.abdominalTitleList()[position]
                }
                context.resources.getString(R.string.knee_artho) -> {
                    tvItem.text = SchoolApp.totalKneeList()[position]
                }
                context.resources.getString(R.string.hip_artho) -> {
                    tvItem.text = SchoolApp.totalHipList()[position]
                }
            }

        }
    }
}
package com.io.pnbschool.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.io.pnbschool.R
import com.io.pnbschool.base.BaseFragment
import com.io.pnbschool.databinding.FragmentProcedureBinding

@Suppress("UNCHECKED_CAST")
class ProcedureFragment : BaseFragment<FragmentProcedureBinding>(){
    lateinit var mBundle: Bundle
    lateinit var hashMap: HashMap<String, String>

    override val layoutRes: Int
        get() = R.layout.fragment_procedure

    fun newInstance(args: Bundle): Fragment {
        val frag = ProcedureFragment()
        frag.arguments = args
        return frag

    }


    @SuppressLint("UseRequireInsteadOfGet")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBundle = arguments!!
        this.hashMap = HashMap()
        var toolbarTitle = ""
        hashMap = mBundle.getSerializable("HashMap") as HashMap<String, String>
        for (element in hashMap.keys) {
            toolbarTitle = element
        }


        binding.apply {
                s = toolbarTitle
                string = hashMap[toolbarTitle]
        }
        binding.ivBack.setOnClickListener {
            val manager = requireActivity().supportFragmentManager
            manager.popBackStackImmediate()
        }

    }


}
package com.io.pnbschool.ui.video

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.io.pnbschool.R
import com.io.pnbschool.application.SchoolApp
import com.io.pnbschool.databinding.ActivityPlayVideoBinding
import com.io.pnbschool.utils.AppConstants


class VideoPlayerActivity : YouTubeBaseActivity() {

    lateinit var binding: ActivityPlayVideoBinding


    var onInitializedListener: YouTubePlayer.OnInitializedListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        SchoolApp.component().inject(this)
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_play_video)



        _initialVideo()
    }

   private fun _initialVideo() {
        binding.toolbarText = intent.extras!!.getString(AppConstants.TAB_TYPE)

        var videoUrl = ""
        when (binding.toolbarText) {
            "Adductor Canal" -> {
                videoUrl = "eLXq1YzvqgI"
            }
            "ESP" -> {
                videoUrl = "0pjwmUa7zIk"
            }
            "Femoral" -> {
                videoUrl = "GRbHtf1FI98"
            }
            "FICB" -> {
                videoUrl = "ficLDusGcZg"
            }
            "Interscalene" -> {
                videoUrl = "ykaDMsUIjEs"
            }
            "IPACK" -> {
                videoUrl = "5WRbeas1k-4"

            }

            "Popliteal Sciatic" -> {
                videoUrl = "r10CCqqiUU4"

            }
            "QL" -> {
                videoUrl = "xr7Dm4nBBAE"

            }
            "Rectus" -> {
                videoUrl = "kv0YUMnwWME"
            }
            "Supraclavicular" -> {
                videoUrl = "8dmYkNOEUn8"

            }
            "Tap" -> {
                videoUrl = "bzqc8HqcdT0"
            }
        }

        binding.ivBack.setOnClickListener {
            onBackPressed()
        }
        playYoutubeVideo(videoUrl)
    }

  private fun playYoutubeVideo(video: String) {
        onInitializedListener = object : YouTubePlayer.OnInitializedListener {
            @SuppressLint("LogNotTimber")
            override fun onInitializationSuccess(
                provider: YouTubePlayer.Provider,
                youTubePlayer: YouTubePlayer,
                b: Boolean
            ) {
                youTubePlayer.setFullscreen(true)
                youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL)
                Log.e("video", "ready for playing")
                youTubePlayer.cueVideo(video)
                youTubePlayer.play()
            }

            override fun onInitializationFailure(
                provider: YouTubePlayer.Provider,
                youTubeInitializationResult: YouTubeInitializationResult
            ) {
                if (youTubeInitializationResult.isUserRecoverableError) {
                    Toast.makeText(
                        this@VideoPlayerActivity,
                        "There is an issue",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        binding.youtubeVideoPlayer.initialize(SchoolApp.YOUTUBE_API_KEY, onInitializedListener)
        binding.youtubeVideoPlayer.keepScreenOn = true


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun playWebVideo() {

    }

}
package com.io.pnbschool.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.io.pnbschool.R
import com.io.pnbschool.base.BaseFragment
import com.io.pnbschool.databinding.FragmentNavigationItemBinding
import com.io.pnbschool.ui.home.MainActivity
import com.io.pnbschool.utils.AppConstants


class FragmentNavigationItems : BaseFragment<FragmentNavigationItemBinding>() {
    override val layoutRes: Int
        get() = R.layout.fragment_navigation_item

    var toolbarTitle = ""


    @SuppressLint("NewApi")
    fun newInstance(args: Bundle): Fragment {
        val frag = FragmentNavigationItems()
        frag.arguments = args
        return frag

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _init()

    }

    private fun _init() {
        val b = this.arguments
        toolbarTitle = b!!.getString(AppConstants.TOOLBAR_TITLE)!!

        binding.toolbarTitle = toolbarTitle
//        binding.lis

        val htmlText = b.getString(AppConstants.HTML)
        if(b.getString(AppConstants.SEND_STRING) == "Multimodal"){
            binding.wvNavigation.loadUrl("$htmlText")
        }else{
            binding.wvNavigation.loadUrl("$htmlText")
        }

        binding.ivBack.setOnClickListener {
            if(requireActivity() is MainActivity){
                (requireActivity() as MainActivity).showNav()
            }
            val manager = requireActivity().supportFragmentManager
            manager.popBackStackImmediate()
            viewModel.observer.extraOpenfragment = viewModel.observer.extraOpenfragment - 1


        }
    }



}
package com.io.pnbschool.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.io.pnbschool.R
import com.io.pnbschool.base.BaseFragment
import com.io.pnbschool.databinding.FragmentMainBinding
import com.io.pnbschool.ui.adapter.SurgeryInfoAdapter
import com.io.pnbschool.utils.AppConstants

class Homefragment :BaseFragment<FragmentMainBinding>(){

    lateinit var madapter:SurgeryInfoAdapter



    override val layoutRes: Int
        get() = R.layout.fragment_main


    override  fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel=bindViewModel {
        }
        Home()
    }

   private fun Home(){
        setUpAdapter()
        binding.apply {
            rvMain.layoutManager=GridLayoutManager(activity,2)
            adapter=madapter
        }
    }

    fun setUpAdapter(){
        madapter= SurgeryInfoAdapter(AppConstants.MainInfo)
    }

}
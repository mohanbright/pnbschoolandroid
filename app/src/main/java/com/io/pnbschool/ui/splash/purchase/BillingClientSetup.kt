package com.io.pnbschool.ui.splash.purchase

import android.content.Context
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.PurchasesUpdatedListener

class BillingClientSetup {
    companion object{
          var  instance:BillingClient ? = null

        fun create(context:Context, listener: PurchasesUpdatedListener): BillingClient {
             if(instance==null){
                instance=setUpBillingClient(context,listener)
            }
            return instance as BillingClient
        }

        fun setUpBillingClient(context:Context, listener: PurchasesUpdatedListener):BillingClient{
            var client  =BillingClient.newBuilder(context).enablePendingPurchases().setListener(listener).build()
            return client
        }
    }


}
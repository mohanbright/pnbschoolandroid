package com.io.pnbschool.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.io.pnbschool.R
import com.io.pnbschool.databinding.ItemGridBinding
import com.io.pnbschool.eventbus.MainEvent
import com.io.pnbschool.utils.AppConstants
import com.io.pnbschool.utils.AppConstants.ImageHashMap
import com.io.pnbschool.utils.AppConstants.surgeryList
import org.greenrobot.eventbus.EventBus

class SurgeryInfoAdapter(var type: String) :
    RecyclerView.Adapter<SurgeryInfoAdapter.SurgeryViewHolder>() {


    /**
     * ViewHolder of adapter
     */
    class SurgeryViewHolder(var binding: ItemGridBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SurgeryViewHolder {
        val binding = DataBindingUtil.inflate<ItemGridBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_grid,
            parent,
            false
        )
        return SurgeryViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return if (type == AppConstants.MainInfo) {
            surgeryList().size
        } else {
            ImageHashMap()[type]!!.size
        }

    }

    override fun onBindViewHolder(holder: SurgeryViewHolder, position: Int) {
        holder.binding.apply {
            if (type == AppConstants.MainInfo) {
                ivItems.setImageResource(surgeryList()[position].imageSrc)
                root.setOnClickListener {
                    EventBus.getDefault().post(MainEvent(position,""))
                }
            } else {
                ivItems.setImageResource(ImageHashMap()[type]!![position])
            }

        }
    }

}
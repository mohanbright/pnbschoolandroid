package com.io.pnbschool.ui.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.io.pnbschool.R
import com.io.pnbschool.databinding.ImageGridBinding
import com.io.pnbschool.ui.home.MainActivity
import com.io.pnbschool.utils.AppConstants.ImageHashMap

class ImagesInfoAdapter(var context:Context,var type: String) : RecyclerView.Adapter<ImagesInfoAdapter.ImageViewHolder>() {


    /**
     * ViewHolder of adapter
     */
    class ImageViewHolder(var binding: ImageGridBinding) : RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ImageViewHolder {
             val binding = DataBindingUtil.inflate<ImageGridBinding>(
                LayoutInflater.from(parent.context),
                R.layout.image_grid,
                parent,
                false
            )
            return ImageViewHolder(binding)

    }

    override fun getItemCount(): Int {
            return ImageHashMap()[type]!!.size
    }
/*CardView
*  */
    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.binding.apply {
            //                EventBus.getDefault().post(ImageHashMap()[type]!!)
//            when {
//                ImageHashMap()[type]!![position]==R.drawable.ficblabeled -> {
//                    ivItems.scaleX=1.3f
//                }
//                ImageHashMap()[type]!![position]==R.drawable.interscaleneapproach -> {
//                    ivItems.scaleType=ImageView.ScaleType.CENTER_CROP
//
//                }
//                ImageHashMap()[type]!![position]==R.drawable.popliteal_internal ->{
//                    ivItems.scaleType=ImageView.ScaleType.CENTER_CROP
//
//                }ImageHashMap()[type]!![position]==R.drawable.rectus2 ->{
//                    ivItems.scaleType=ImageView.ScaleType.CENTER_CROP
//
//                }ImageHashMap()[type]!![position]==R.drawable.aductor2 ->{
//                ivItems.scaleX=1.3f
//                ivItems.scaleY=1.1f
//
//            }ImageHashMap()[type]!![position]==R.drawable.ipacklabeled -> {
//                    ivItems.scaleX=1.2f
//            }ImageHashMap()[type]!![position]==R.drawable.posteriorqlapproach -> {
//                ivItems.scaleX=1.2f
//                ivItems.scaleY=1.3f
//            }
//            }
            /*     android:scaleX="1.2"
            android:scaleY="1.3"
            android:src="@drawable/posteriorqlapproach" */
            ivItems.setImageResource(ImageHashMap()[type]!![position])
            root.setOnClickListener {
                if(context is MainActivity){
                    (context as MainActivity).showSlider(type,ImageHashMap()[type]!!)
                }
            }


        }
    }

}
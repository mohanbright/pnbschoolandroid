package com.io.pnbschool.ui.splash

import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.android.billingclient.api.*
import com.io.pnbschool.R
import com.io.pnbschool.application.SchoolApp
import com.io.pnbschool.base.BaseActivity
import com.io.pnbschool.base.DataBindingActivity
import com.io.pnbschool.databinding.ActivitySplashBinding
import com.io.pnbschool.ui.home.MainActivity
import org.joda.time.DateTime
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.util.*


class SplashActivity : DataBindingActivity<ActivitySplashBinding>(), PurchasesUpdatedListener {
    lateinit var params: Bundle
    lateinit var billingClient: BillingClient
    lateinit var listener: ConsumeResponseListener
    private var skuList: List<String> = ArrayList()
    private var skuDetailsList: List<SkuDetails>? = null
    private lateinit var toPurchase: String


    override val layoutRes: Int
        get() = R.layout.activity_splash

    override fun onCreate(savedInstanceState: Bundle?) {
        SchoolApp.component().inject(this)
        activity = this@SplashActivity
        super.onCreate(savedInstanceState)
        viewModel = bindViewModel {
            vm = viewModel
        }
        _initialSplash()

    }


    override fun getThisActivity(): BaseActivity = activity as BaseActivity

    private fun _initialSplash() {
        setupBillingClient()

        Looper.myLooper()?.let {
            Handler(it).postDelayed({
                viewModel.splashObserver.isSubscribed = 1
                init()
            }, 5000)
        }
    }

    private fun performAction() {
        startNewActivity(MainActivity::class.java)
    }


    private fun init() {
        skuList = listOf(resources.getString(R.string.monthly_subscription))
        binding.btnSubscribe.setOnClickListener {
            purchaseInit()

        }
    }


    private fun setupBillingClient() {
        billingClient = BillingClient.newBuilder(this)
            .enablePendingPurchases()
            .setListener(this)
            .build()
        billingClient.startConnection(object : BillingClientStateListener {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onBillingSetupFinished(billingResult: BillingResult) {
                if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                    loadAllSKUs()
                    val a = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
                    if (a.purchasesList != null) {
                        for (i in a.purchasesList!!) {
                            if (i.sku == resources.getString(R.string.monthly_subscription)) {
//                                showMessage("purchased")
                                performAction()
                            } else {
//                                showMessage("Not Purchased")
                            }
                        }
                    }

                }
            }

            override fun onBillingServiceDisconnected() {
                showMessage("Failed")
//                KoinApplication.logger.error("Failed")

            }
        })
    }


    override fun onPurchasesUpdated(
        billingResult: BillingResult,
        purchases: MutableList<Purchase>?
    ) {
        if (!(billingResult.responseCode != BillingClient.BillingResponseCode.OK || purchases == null)) {
            for (purchase in purchases) {
                if (!purchase.isAcknowledged) {
                    acknowledgePurchase(purchase.purchaseToken)
                } else {
                    Toast.makeText(this, "Purchase Successful", Toast.LENGTH_SHORT).show()
                    performAction()
                }
            }
        } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            Toast.makeText(this, "Purchase Cancelled", Toast.LENGTH_SHORT).show()

        } else {

            Toast.makeText(this, "An Error Occurred!", Toast.LENGTH_SHORT).show()
        }
    }


    private fun loadAllSKUs() = if (billingClient.isReady) {
        val params = SkuDetailsParams
            .newBuilder()
            .setSkusList(skuList)

            .setType(BillingClient.SkuType.SUBS)
            .build()

        billingClient.querySkuDetailsAsync(params) { billingResult, skuDetailsList ->
            // Process the result.
            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK &&
                skuDetailsList!!.isNotEmpty()
            ) {
                this.skuDetailsList = skuDetailsList

            }

        }

    } else {
        println("Billing Client not ready")
    }

    private fun acknowledgePurchase(purchaseToken: String) {
        val params = AcknowledgePurchaseParams.newBuilder()
            .setPurchaseToken(purchaseToken)
            .build()
        val acknowledgePurchaseResponseListener = AcknowledgePurchaseResponseListener {
            performAction()
        }
        billingClient.acknowledgePurchase(params, acknowledgePurchaseResponseListener)
    }


    private fun purchaseInit() {
        toPurchase = resources.getString(R.string.monthly_subscription)
//        if (skuDetailsList == null) {
//            Toast.makeText(this, "Billing isn't ready", Toast.LENGTH_SHORT).show()
//        } else {
        val params = SkuDetailsParams
            .newBuilder()
            .setSkusList(skuList)
            .setType(BillingClient.SkuType.SUBS)
            .build()
        billingClient.querySkuDetailsAsync(params) { billingResult, skuDetailsListt ->

            for (skuDetails in skuDetailsListt!!) {
                if (skuDetails.sku == toPurchase) {
                    val billingFlowParams = BillingFlowParams
                        .newBuilder()
                        .setSkuDetails(skuDetails)
                        .build()
                    billingClient.launchBillingFlow(this, billingFlowParams)
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun handlePurchased(
        expiryTime: String
    ) {

        var numOfDays = 30
        val c: Calendar = Calendar.getInstance()
        val dateCurrent = DateTime(expiryTime.toLong())
        c.time = dateCurrent.toDate()

        c.add(Calendar.DATE, numOfDays)
        val expDate: Date = c.time

        val dateTime = DateTime(c)


        var timeInMilliseconds: Long? = null
        try {
            val formatter: DateTimeFormatter =
                DateTimeFormatter.ofPattern("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
            val localDate: LocalDateTime =
                LocalDateTime.parse(dateTime.toDate().toString(), formatter)
            timeInMilliseconds = localDate.atOffset(ZoneOffset.UTC).toInstant().toEpochMilli()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        showMessage("App Subscription over")


    }


}



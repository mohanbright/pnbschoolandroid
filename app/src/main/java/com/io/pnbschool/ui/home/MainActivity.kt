package com.io.pnbschool.ui.home

import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import com.android.billingclient.api.BillingClient
import com.google.android.material.navigation.NavigationView
import com.io.pnbschool.R
import com.io.pnbschool.application.SchoolApp
import com.io.pnbschool.base.BaseActivity
import com.io.pnbschool.base.DataBindingActivity
import com.io.pnbschool.databinding.ActivityMainBinding
import com.io.pnbschool.eventbus.MainEvent
import com.io.pnbschool.ui.fragment.FragmentNavigationItems
import com.io.pnbschool.ui.fragment.HomeChildFragment
import com.io.pnbschool.ui.fragment.Homefragment
import com.io.pnbschool.ui.fragment.ProcedureFragment
import com.io.pnbschool.utils.AppConstants
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@Suppress("VARIABLE_WITH_REDUNDANT_INITIALIZER")
class MainActivity : DataBindingActivity<ActivityMainBinding>(),
    NavigationView.OnNavigationItemSelectedListener {


    lateinit var mainViewModel: HomeVM
    lateinit var mainFragment: Homefragment
    lateinit var toggle: ActionBarDrawerToggle
    var childFragment: HomeChildFragment = HomeChildFragment()
    var navigationFragment: FragmentNavigationItems = FragmentNavigationItems()
    var procedureFragment: ProcedureFragment = ProcedureFragment()


    override fun onCreate(savedInstanceState: Bundle?) {
        SchoolApp.component().inject(this)
        activity = this@MainActivity
        super.onCreate(savedInstanceState)
        mainViewModel = bindViewModel {
            homeViewModel = it
        }
        _initialHome()
    }

    override fun getThisActivity(): BaseActivity = activity as BaseActivity


    fun _initialHome() {

        toggle = ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            binding.toolbar,
            R.string.drawer_open,
            R.string.drawer_close
        )
        toggle.syncState()
        setSupportActionBar(binding.toolbar)

        binding.apply {
            navView.setNavigationItemSelectedListener(this@MainActivity)
        }

        showNav()
        _inflateHome()
    }

    override fun onResume() {
        super.onResume()
        Log.e("lifecycle:", "onResume")
    }


    override val layoutRes: Int
        get() = R.layout.activity_main

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        var bundle = Bundle()
        when (item.itemId) {
            R.id.nav_surgary -> {

                bundle = Bundle()
                bundle.putString(AppConstants.SEND_STRING, resources.getString(R.string.surgary))
                bundle.putString(AppConstants.TOOLBAR_TITLE, resources.getString(R.string.surgary))
                bundle.putString(AppConstants.HTML, AppConstants.ABDOMINAL_SURGARY_CONTENT)
                navigationFragment = FragmentNavigationItems()
                loadNextFragment(R.id.child_container, navigationFragment.newInstance(bundle))
                hideNav()
                _fetchDrawerGravity()
                return true
            }
            R.id.nav_photos -> {

                bundle = Bundle()

                bundle.putString(AppConstants.SEND_STRING, resources.getString(R.string.knee_artho))
                bundle.putString(
                    AppConstants.TOOLBAR_TITLE,
                    resources.getString(R.string.knee_artho)
                )
                bundle.putString(AppConstants.HTML, AppConstants.KNEE_ARTHO_CONTENT)
                navigationFragment = FragmentNavigationItems()
                loadNextFragment(R.id.child_container, navigationFragment.newInstance(bundle))
                hideNav()
                _fetchDrawerGravity()
                return true
            }
            R.id.nav_movies -> {

                bundle = Bundle()
                bundle.putString(AppConstants.HTML, AppConstants.HIP_AURTHO_CONTENT)
                bundle.putString(AppConstants.SEND_STRING, resources.getString(R.string.hip_artho))
                bundle.putString(
                    AppConstants.TOOLBAR_TITLE,
                    resources.getString(R.string.hip_artho)
                )
                navigationFragment = FragmentNavigationItems()
                loadNextFragment(R.id.child_container, navigationFragment.newInstance(bundle))
                hideNav()
                _fetchDrawerGravity()
                return true
            }
            R.id.nav_notifications -> {

                bundle = Bundle()
                bundle.putString(AppConstants.HTML, AppConstants.TERMS_CONTENT)
                bundle.putString(AppConstants.SEND_STRING, resources.getString(R.string.terms))
                bundle.putString(AppConstants.TOOLBAR_TITLE, resources.getString(R.string.terms))
                navigationFragment = FragmentNavigationItems()
                loadNextFragment(R.id.child_container, navigationFragment.newInstance(bundle))
                hideNav()
                _fetchDrawerGravity()
                return true
            }
            R.id.nav_settings -> {

                bundle = Bundle()
                bundle.putString(AppConstants.SEND_STRING, resources.getString(R.string.us))
                bundle.putString(AppConstants.TOOLBAR_TITLE, resources.getString(R.string.us))
                bundle.putString(AppConstants.HTML, AppConstants.ABOUTUS_HTML)
                navigationFragment = FragmentNavigationItems()
                loadNextFragment(R.id.child_container, navigationFragment.newInstance(bundle))
                hideNav()
                _fetchDrawerGravity()
                return true
            }
        }
        return false
    }

    fun _inflateHome() {
        mainFragment = Homefragment()
        loadFragment(R.id.content_container, mainFragment)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun callEvent(mainEvent: MainEvent) {
        if (mainEvent.pos == 11) {
            val bundle = Bundle()
            bundle.putString(AppConstants.SEND_STRING, "Multimodal")
            bundle.putString(
                AppConstants.TOOLBAR_TITLE,
                AppConstants.surgeryList()[mainEvent.pos].toolbarTitle
            )
            bundle.putString(AppConstants.HTML, AppConstants.MULTIMODAL_MEDS_CONTENT)
            navigationFragment = FragmentNavigationItems()
            loadNextFragment(R.id.child_container, navigationFragment.newInstance(bundle))

        } else {
            childFragment = HomeChildFragment()
            val bundle = Bundle()
            bundle.putString(
                AppConstants.SEND_STRING,
                AppConstants.surgeryList()[mainEvent.pos].title
            )
            bundle.putString(
                AppConstants.TOOLBAR_TITLE,
                AppConstants.surgeryList()[mainEvent.pos].toolbarTitle
            )
            loadNextFragment(R.id.child_container, childFragment.newInstance(bundle))
        }
//        _fetchDrawerGravity()
//        hideNav()
    }

    override fun onBackPressed() {
        Log.e("onBackPressed", "${viewModel.observer.extraOpenfragment}")
        if (viewModel.observer.extraOpenfragment <= 0) {
            super.onBackPressed()
        } else {
            showNav()
            supportFragmentManager.popBackStack()
            viewModel.observer.extraOpenfragment = viewModel.observer.extraOpenfragment - 1
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (toggle.onOptionsItemSelected(item)) {
            return true
        }

        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                showNav()
                return true

            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun hideNav() {
        toggle.isDrawerIndicatorEnabled = false

    }

    fun showNav() {
        toggle.isDrawerIndicatorEnabled = true
    }

    fun _fetchDrawerGravity() {
        if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.openDrawer(GravityCompat.START)
        } else {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun openNextFragment(bundle: Bundle) {
        Log.e("MainActivity", "CLick")
        loadNextFragment(R.id.child_container, procedureFragment.newInstance(bundle))

    }

    fun showSlider(titleText: String, images: List<Int>) {
        showSliderDialog(titleText, images)
    }




}

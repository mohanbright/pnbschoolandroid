package com.io.pnbschool.dagger.Module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.io.pnbschool.base.BaseVM
import com.io.pnbschool.dagger.BaseViewModelKey
import com.io.pnbschool.dagger.factory.ViewModelKey
import com.io.pnbschool.dagger.factory.ViewModelProvideFactory
import com.io.pnbschool.ui.home.HomeVM
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule{
     @Binds
     abstract fun bindViewModelFactory(baseViewModelFactory: ViewModelProvideFactory) : ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeVM::class)
    protected abstract fun bindHomeVM(homeVM: HomeVM) : ViewModel

}

package com.io.pnbschool.dagger.Module

import androidx.lifecycle.ViewModelProvider
import com.io.pnbschool.dagger.factory.ViewModelProvideFactory
import dagger.Binds
import dagger.Module

@Module(includes = [ViewModelModule::class])
abstract class ActivityModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelProvideFactory): ViewModelProvider.Factory

}
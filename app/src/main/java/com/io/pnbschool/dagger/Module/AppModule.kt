package com.io.pnbschool.dagger.Module

import android.content.Context
import com.io.pnbschool.application.SchoolApp
import com.io.pnbschool.dagger.component.ActivityComponent
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

//@Module(includes = [ActivityModule::class],
//subcomponents = [ActivityComponent::class])
@Module
class AppModule {

    @Provides
    @Singleton
    fun provideApp(): SchoolApp = SchoolApp.getInstance()


    @Provides
    @Singleton
    fun provideContext(app: SchoolApp): Context =  app.getApplicationContext()





}
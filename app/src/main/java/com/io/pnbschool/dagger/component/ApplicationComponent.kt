package com.io.pnbschool.dagger.component

import android.app.Application
import com.io.pnbschool.application.SchoolApp
import com.io.pnbschool.base.BaseActivity
import com.io.pnbschool.base.DataBindingActivity
import com.io.pnbschool.dagger.Module.AppModule
import com.io.pnbschool.dagger.Module.ViewModelModule
import com.io.pnbschool.ui.home.MainActivity
import com.io.pnbschool.ui.splash.SplashActivity
import com.io.pnbschool.ui.video.VideoPlayerActivity
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class,ViewModelModule::class))
@Singleton
interface ApplicationComponent {

    fun inject(application: SchoolApp) : SchoolApp
    fun inject(baseActivity: BaseActivity) : BaseActivity
    fun inject(splashActivity: SplashActivity) : SplashActivity
    fun inject(splashActivity: MainActivity) : MainActivity
    fun inject(splashActivity: VideoPlayerActivity) : VideoPlayerActivity


    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder?
        fun build(): ApplicationComponent

    }


}
package com.io.pnbschool.application

import android.app.Application
import com.facebook.FacebookSdk
import com.facebook.LoggingBehavior
import com.facebook.appevents.AppEventsLogger

import com.io.pnbschool.dagger.component.ApplicationComponent
import com.io.pnbschool.dagger.component.DaggerApplicationComponent



class SchoolApp : Application() {

//AppEventsLogger logger = AppEventsLogger.newLogger(this);

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        appComponent = DaggerApplicationComponent.builder().application(this)!!.build()
        appComponent.inject(mInstance)
        initFacebook()

    }

   public fun initFacebook(){
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()

//        FacebookSdk.sdkInitialize(applicationContext);
//        AppEventsLogger.activateApp(this)
//        FacebookSdk.fullyInitialize()
//        FacebookSdk.setAutoLogAppEventsEnabled(true)
//        FacebookSdk.setAutoInitEnabled(true)
//        FacebookSdk.setIsDebugEnabled(true)
//        FacebookSdk.addLoggingBehavior(LoggingBehavior.APP_EVENTS);
//        FacebookSdk.setAdvertiserIDCollectionEnabled(true)
//        logger= AppEventsLogger.newLogger(this)
    }

    companion object {
        lateinit var mInstance: SchoolApp
        lateinit var appComponent: ApplicationComponent
//        lateinit var logger:AppEventsLogger


        var YOUTUBE_API_KEY="AIzaSyBG63mHMrdtBPwa5Knc-viSc8xHuE5HJq0"


        @Synchronized
        fun getInstance(): SchoolApp = mInstance

        @Synchronized
        fun component(): ApplicationComponent = appComponent

        /**
         * Andominal Navigation Titles
         */
        fun abdominalTitleList(): List<String> {
            val arrayList = ArrayList<String>()
            arrayList.add("Premedication")
            arrayList.add("Peripheral Nerve Blocks")
            arrayList.add("Peripheral Nerve Blocks")
            arrayList.add("Anesthetic Management")
            return arrayList
        }

        /**
         * Andominal Total knee Titles
         */
        fun totalKneeList(): List<String> {
            val arrayList = ArrayList<String>()
            arrayList.add("Premedication")
            arrayList.add("Ultrasound Guided Peripheral Nerve Blocks (in preop)")
            arrayList.add("Anesthetic Management (choice of GA or Spinal)")
            arrayList.add("Anesthetic Management")
            return arrayList
        }

        /**
         * Andominal Total Hip Titles
         */
        fun totalHipList(): List<String> {
            val arrayList = ArrayList<String>()
            arrayList.add("Premedication")
            arrayList.add("Ultrasound Guided Peripheral Nerve Blocks")
            arrayList.add("Anesthetic Management (choice of GA or Spinal)")
            return arrayList
        }

    }



}
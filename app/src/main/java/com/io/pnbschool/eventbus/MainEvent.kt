package com.io.pnbschool.eventbus

data class MainEvent(
    var pos: Int,
    var type : String
)
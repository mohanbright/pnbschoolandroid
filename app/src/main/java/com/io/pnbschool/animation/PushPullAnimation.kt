package com.io.pnbschool.animation

import android.view.animation.Transformation
import androidx.annotation.IntDef
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

open class PushPullAnimation private constructor(
    @field:Direction @param:Direction protected val mDirection: Int,
    protected val mEnter: Boolean,
    duration: Long
) : ViewPropertyAnimation() {
    @IntDef(
        UP,
        DOWN,
        LEFT,
        RIGHT
    )
    @Retention(RetentionPolicy.SOURCE)
    internal annotation class Direction

    private class VerticalPushPullAnimation(
        @Direction direction: Int,
        enter: Boolean,
        duration: Long
    ) : PushPullAnimation(direction, enter, duration) {
        override fun initialize(
            width: Int,
            height: Int,
            parentWidth: Int,
            parentHeight: Int
        ) {
            super.initialize(width, height, parentWidth, parentHeight)
            mPivotX = width * 0.5f
            mPivotY =
                if (mEnter == (mDirection == DOWN)) 0.0f else height.toFloat()
        }

        override fun applyTransformation(
            interpolatedTime: Float,
            t: Transformation
        ) {
            var value = if (mEnter) interpolatedTime - 1.0f else interpolatedTime
            if (mDirection == UP) value *= -1.0f
            mRotationX = value * 90.0f
            mAlpha = if (mEnter) interpolatedTime else 1.0f - interpolatedTime
            super.applyTransformation(interpolatedTime, t)
            applyTransformation(t)
        }
    }

    /**
     *
     */
    private class HorizontalPushPullAnimation(
        @Direction direction: Int,
        enter: Boolean,
        duration: Long
    ) : PushPullAnimation(direction, enter, duration) {
        override fun initialize(
            width: Int,
            height: Int,
            parentWidth: Int,
            parentHeight: Int
        ) {
            super.initialize(width, height, parentWidth, parentHeight)
            mPivotX =
                if (mEnter == (mDirection == RIGHT)) 0.0f else width.toFloat()
            mPivotY = height * 0.5f
        }

        override fun applyTransformation(
            interpolatedTime: Float,
            t: Transformation
        ) {
            var value = if (mEnter) interpolatedTime - 1.0f else interpolatedTime
            if (mDirection == LEFT) value *= -1.0f
            mRotationY = -value * 90.0f
            mAlpha = if (mEnter) interpolatedTime else 1.0f - interpolatedTime
            super.applyTransformation(interpolatedTime, t)
            applyTransformation(t)
        }
    }

    companion object {
        const val UP = 1
        const val DOWN = 2
        const val LEFT = 3
        const val RIGHT = 4

        /**
         * Create new Animation.
         * @param direction Direction of animation
         * @param enter true for Enter / false for Exit
         * @param duration Duration of Animation
         * @return
         */
        fun create(
            @Direction direction: Int,
            enter: Boolean,
            duration: Long
        ): PushPullAnimation {
            return when (direction) {
                UP, DOWN -> VerticalPushPullAnimation(
                    direction,
                    enter,
                    duration
                )
                LEFT, RIGHT -> HorizontalPushPullAnimation(
                    direction,
                    enter,
                    duration
                )
                else -> HorizontalPushPullAnimation(direction, enter, duration)
            }
        }
    }

    init {
        setDuration(duration)
    }
}
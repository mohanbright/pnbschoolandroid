package com.io.pnbschool.base

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.io.pnbschool.dagger.factory.ViewModelProvideFactory
import javax.inject.Inject

abstract class BaseFragment<TBinding : ViewDataBinding>:Fragment(),BaseInterface{
    lateinit var baseActivity: BaseActivity

    public lateinit var viewModel: BaseVM

    @Inject
    public lateinit var viewModelFactory: ViewModelProvideFactory




    abstract val layoutRes: Int
        @LayoutRes get


    protected lateinit var binding: TBinding



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        baseActivity= currentActiity()
        binding=DataBindingUtil.inflate(inflater,layoutRes,container,false)
        viewModel=baseActivity.getViewModel(BaseVM::class.java)
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        return super.onCreateAnimation(transit, enter, nextAnim)
    }



    override fun currentActiity(): BaseActivity = activity as BaseActivity




    protected inline fun <reified T : ViewModel> bindViewModel(
        noinline f: (TBinding.(T) -> Unit)? = null
    ): T {
        val viewModel = baseActivity.getViewModel(T::class.java)
        f?.invoke(binding, viewModel)
        binding.executePendingBindings()
        return viewModel
    }


    override  fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    override fun showMessage(message: String) {
        baseActivity.showMessage(message)
    }

    override fun <T : Activity> startNewActivity(java: Class<T>) {
        baseActivity.startNewActivity(java)
    }

    override fun <T : Activity> startActivityWithoutFinish(c: Class<T>) {
        baseActivity.startActivityWithoutFinish(c)
    }

    override fun loadFragment(id: Int, fragment: Fragment) {
        baseActivity.loadFragment(id,fragment)
    }

    override fun loadNextFragment(id: Int, fragment: Fragment) {
        baseActivity.loadNextFragment(id,fragment)
    }

    override fun showSliderDialog(title:String,images: List<Int>) {
        baseActivity.showSliderDialog(title,images)
    }

    override fun <T : Activity> startActivityWithPutExtra(c: Class<T>, type: String) {
        baseActivity.startActivityWithPutExtra(c,type)
    }

}
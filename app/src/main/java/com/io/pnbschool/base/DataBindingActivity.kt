package com.io.pnbschool.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModel
import com.io.pnbschool.eventbus.DefaultEvent
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

@Suppress("UNUSED_PARAMETER")
abstract class DataBindingActivity<TBinding : ViewDataBinding> : BaseActivity(){

    abstract val layoutRes: Int
        @LayoutRes get

    override fun onStart() {
        super.onStart()
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
   public fun defaultEvent(event:DefaultEvent){

    }
    protected lateinit var binding: TBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity=this@DataBindingActivity
        binding = DataBindingUtil.setContentView(this, layoutRes)
        binding.lifecycleOwner = this
    }

    /**
     * Creates a [ViewModel] and binds it to the [ViewDataBinding] for this view.
     */
    protected inline fun <reified T : ViewModel> bindViewModel(
        noinline f: (TBinding.(T) -> Unit)? = null
    ): T {
        val viewModel = getViewModel(T::class.java)
        f?.invoke(binding, viewModel)
        binding.executePendingBindings()
        return viewModel
    }

    override fun getThisActivity(): BaseActivity =activity as BaseActivity

}

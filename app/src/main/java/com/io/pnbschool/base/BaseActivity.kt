package com.io.pnbschool.base

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.facebook.FacebookSdk
import com.io.pnbschool.R
import com.io.pnbschool.dagger.factory.ViewModelProvideFactory
import com.io.pnbschool.ui.adapter.SlidingImageAdapter
import com.io.pnbschool.utils.AppConstants
import kotlinx.android.synthetic.main.images_dialog.*
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
public abstract class BaseActivity : AppCompatActivity(), BaseInterface {


    public lateinit var viewModel: BaseVM
    public lateinit var activity: Activity
    var dialog: Dialog? = null

    @Inject
    public lateinit var viewModelFactory: ViewModelProvideFactory


    /**
     * Show message
     */
    override fun showMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity = this@BaseActivity
        viewModel = getViewModel(BaseVM::class.java)

    }


    /***/
    fun <T : ViewModel?> getViewModel(java: Class<T>): T {
        return ViewModelProvider(this, viewModelFactory).get(java)
    }

    override fun <T : Activity> startNewActivity(java: Class<T>) {
        val intent = Intent(this, java)
        startActivity(intent)
        finish()
    }

    override fun <T : Activity> startActivityWithoutFinish(activity: Class<T>) {
        val intent = Intent(this, activity)
        startActivity(intent)
    }

    override fun loadFragment(id: Int, fragment: Fragment) {
        val fragmentTransaction =
            supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(id, fragment)
        if (fragment.isAdded) {
            return
        }
        fragmentTransaction.commit()
    }

    override fun loadNextFragment(id: Int, fragment: Fragment) {
        val backStateName: String = fragment.javaClass.name
        val fragmentTransaction =
            supportFragmentManager.beginTransaction()
//        fragmentTransaction.setCustomAnimations(
//            R.anim.slide_in_up,
//            R.anim.slide_in_down,
//            R.anim.slide_out_down,
//            R.anim.slide_out
//        )
        fragmentTransaction.replace(id, fragment)
        if (fragment.isAdded) {
            return
        }
        fragmentTransaction.addToBackStack(backStateName)
        fragmentTransaction.commit()
        viewModel.observer.extraOpenfragment = viewModel.observer.extraOpenfragment + 1

    }

    override fun currentActiity(): BaseActivity {
        return activity as BaseActivity
    }

    protected abstract fun getThisActivity(): BaseActivity


    override fun showSliderDialog(titleText: String, images: List<Int>) {
        dialog = Dialog(activity)
        dialog?.apply {
            setCancelable(false)
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setContentView(R.layout.images_dialog)

            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

            val viewPager = findViewById<ViewPager>(R.id.viewpager)
            val cancelImage = findViewById<ImageView>(R.id.iv_header)

            tv_header.text = titleText
            cancelImage.setOnClickListener {
                dismiss()
            }

            viewPager.adapter = SlidingImageAdapter(getThisActivity(), images)
        }

        dialog!!.show()
    }

    override fun <T : Activity> startActivityWithPutExtra(c: Class<T>, type: String) {
        val intent = Intent(this, c)
        intent.putExtra(AppConstants.TAB_TYPE, type)
        startActivity(intent)

    }


}
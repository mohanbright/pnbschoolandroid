package com.io.pnbschool.base

import androidx.databinding.BaseObservable
import androidx.lifecycle.ViewModel

abstract class BaseVM () : ViewModel() {


    var observer: HomeObserver = HomeObserver()
    var splashObserver: SplashObserver = SplashObserver()

    inner class HomeObserver : BaseObservable() {
        var extraOpenfragment = 0
            set(value) {
                field = value
                notifyChange()
            }
    }

    inner class SplashObserver :BaseObservable(){
        var isSubscribed =0
            set(value) {
                field = value
                notifyChange()
            }

    }
}
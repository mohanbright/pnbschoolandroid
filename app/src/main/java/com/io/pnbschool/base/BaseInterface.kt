package com.io.pnbschool.base

import android.app.Activity
import androidx.fragment.app.Fragment

abstract interface BaseInterface {

    fun  showMessage(message:String)
    fun <T : Activity> startNewActivity(java: Class<T>)
    fun <T : Activity> startActivityWithoutFinish(c:Class<T>)

    fun <T : Activity> startActivityWithPutExtra(c:Class<T>,type:String)

    fun loadFragment(id:Int,fragment: Fragment)
    fun loadNextFragment(id:Int,fragment: Fragment)

    fun currentActiity() : BaseActivity

    fun showSliderDialog(title:String,images:List<Int>)




}
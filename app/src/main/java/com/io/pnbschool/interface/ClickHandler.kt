package com.io.pnbschool.`interface`

import android.view.View

interface ClickHandler {

    fun onItemClick(pos:Int)
}

 interface ViewClickListener{
     fun onClick(view:View)
}
package com.io.pnbschool.utils

class Comments {

//package com.io.pnbschool.ui.splash
//
//import android.content.pm.PackageInfo
//import android.content.pm.PackageManager
//import android.os.Bundle
//import android.os.Handler
//import android.os.Looper
//import android.util.Base64
//import android.util.Log
//import com.android.billingclient.api.*
//import com.facebook.applinks.AppLinkData
//import com.io.pnbschool.R
//import com.io.pnbschool.application.SchoolApp
//import com.io.pnbschool.base.BaseActivity
//import com.io.pnbschool.base.DataBindingActivity
//import com.io.pnbschool.databinding.ActivitySplashBinding
//import com.io.pnbschool.ui.home.MainActivity
//import com.io.pnbschool.ui.splash.purchase.BillingClientSetup
//import kotlinx.android.synthetic.main.activity_splash.*
//import java.security.MessageDigest
//import java.security.NoSuchAlgorithmException
//
//
//class SplashActivity : DataBindingActivity<ActivitySplashBinding>(),
//    PurchasesUpdatedListener, BillingClientStateListener {
//    lateinit var params: Bundle
//    lateinit var billingClient: BillingClient
//    lateinit var listener: ConsumeResponseListener
//    lateinit var _params: SkuDetailsParams
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        SchoolApp.component().inject(this)
//        activity = this@SplashActivity
//        super.onCreate(savedInstanceState)
//        viewModel = bindViewModel {
//            vm = viewModel
//        }
//        _initialSplash()
//    }
//
//    override fun getThisActivity(): BaseActivity = activity as BaseActivity
//
//    fun _initialSplash() {
//        getFacebookHashkey()
//        Looper.myLooper()?.let {
//            Handler(it).postDelayed({
//                bind()
//            }, 5000)
//        }
//    }
//
//    private fun performAction() {
//        Log.e("Calling", "Activity")
//        startNewActivity(MainActivity::class.java)
//    }
//
//    override val layoutRes: Int
//        get() = R.layout.activity_splash
//
//
//    fun getFacebookHashkey() {
//        val info: PackageInfo
//        try {
//            info = packageManager.getPackageInfo("com.io.pnbschool", PackageManager.GET_SIGNATURES)
//            for (signature in info.signatures) {
//                var md: MessageDigest = MessageDigest.getInstance("SHA")
//                md.update(signature.toByteArray())
//                val something = String(Base64.encode(md.digest(), 0))
//                //String something = new Strixng(Base64.encodeBytes(md.digest()));
//                Log.e("facebook ", "hash key $something")
//            }
//        } catch (e1: PackageManager.NameNotFoundException) {
//            Log.e("facebook", "name not found$e1")
//        } catch (e: NoSuchAlgorithmException) {
//            Log.e("facebook", "no such an algorithm $e")
//        } catch (e: Exception) {
//            Log.e("facebook", "exception $e")
//        }
//    }
//
//    fun facebookAds() {
//        SchoolApp.getInstance().initFacebook()
//        AppLinkData.fetchDeferredAppLinkData(this@SplashActivity) {
//            Log.e("SplashActivity", "$it")
//        }
//    }
//
//    /**
//     * click listener
//     */
//    fun subscribe() {
//        if (billingClient.isReady) {
//            _params = SkuDetailsParams.newBuilder().setSkusList(listOf("vvip_2021"))
//                .setType(BillingClient.SkuType.SUBS).build()
//
//            billingClient.querySkuDetailsAsync(
//                _params
//            ) { p0, p1 ->
//                if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
//                    showMessage("${p0.responseCode}")
//                    load(p1!!)
//                } else {
//                    showMessage("Error code ${p0.responseCode} ")
//
//                }
//            }
//        }
//
//    }
//
//    /**
//     *
//     */
//    private fun load(p1: List<SkuDetails>) {
//        val billingFlowParams = BillingFlowParams.newBuilder().setSkuDetails(p1.first()).build()
//
//        val response = billingClient.launchBillingFlow(this, billingFlowParams).responseCode
//        when (response) {
//            BillingClient.BillingResponseCode.BILLING_UNAVAILABLE -> {
//                showMessage("BILLING_UNAVAILABLE")
//            }
//            BillingClient.BillingResponseCode.DEVELOPER_ERROR -> {
//                showMessage("DEVELOPER_ERROR")
//            }
//            BillingClient.BillingResponseCode.FEATURE_NOT_SUPPORTED -> {
//                showMessage("FEATURE_NOT_SUPPORTED")
//            }
//            BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED -> {
//                showMessage("ITEM_ALREADY_OWNED")
//            }
//            BillingClient.BillingResponseCode.SERVICE_DISCONNECTED -> {
//                showMessage("SERVICE_DISCONNECTED")
//            }
//            BillingClient.BillingResponseCode.SERVICE_TIMEOUT -> {
//                showMessage("SERVICE_TIMEOUT")
//            }
//            BillingClient.BillingResponseCode.ITEM_UNAVAILABLE -> {
//                showMessage("ITEM_UNAVAILABLE")
//            }
//        }
//    }
//
//    fun bind() {
//        listener = ConsumeResponseListener { p0, p1 ->
//            if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
//                Log.e("TAG", "Consume OK")
//            }
//        }
//        billingClient = BillingClientSetup.create(this, this)
//        billingClient.startConnection(this)
//        btn_subscribe.setOnClickListener {
//            subscribe()
//        }
//
//    }
//
//
//    override fun onPurchasesUpdated(p0: BillingResult, p1: MutableList<Purchase>?) {
//        if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
//        }
//        Log.e("CODE", "${p0.responseCode}")
//    }
//
//    override fun onBillingServiceDisconnected() {
//        showMessage("disconnect")
//    }
//
//
//    override fun onBillingSetupFinished(p0: BillingResult) {
//
//        if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
//            showMessage("Success to connect")
//            var purchses = billingClient.queryPurchases(BillingClient.SkuType.SUBS).purchasesList
//            purchses?.let {
//                if (it.isNullOrEmpty()) {
//                    viewModel.splashObserver.isSubscribed = 1
//                    handleItemPurchased(it)
//                } else {
//                    performAction()
//                }
//            }
//
//        } else {
//            showMessage("Error code : ${p0.responseCode}")
//        }
//    }
//
//    fun handleItemPurchased(purchses: MutableList<Purchase>) {
//        purchses.forEach {
//            if (it.sku == "vvip_2021") {
//                var param = ConsumeParams.newBuilder().setPurchaseToken(it.purchaseToken).build()
//                billingClient.consumeAsync(param, listener)
//
//                Log.e("TAG", "${it.sku}")
//            }
//        }
//
//    }
//}
}
package com.io.pnbschool.utils

import com.io.pnbschool.R
import com.io.pnbschool.modal.SurgeryDetails

object AppConstants {
    var TOOLBAR_TITLE: String = "toolbar_title"
    val MainInfo: String = "main"
    val HTML: String = "html_filetype"
    val TAB_TYPE: String = "tabType"
    val SEND_STRING: String = "toolbarText"

    fun ImageHashMap(): HashMap<String, List<Int>> {
        val map = HashMap<String, List<Int>>()
        map[surgeryList()[0].title] = addimageViewList(0)
        map[surgeryList()[1].title] = addimageViewList(1)
        map[surgeryList()[2].title] = addimageViewList(2)
        map[surgeryList()[3].title] = addimageViewList(3)
        map[surgeryList()[4].title] = addimageViewList(4)
        map[surgeryList()[5].title] = addimageViewList(5)
        map[surgeryList()[6].title] = addimageViewList(6)
        map[surgeryList()[7].title] = addimageViewList(7)
        map[surgeryList()[8].title] = addimageViewList(8)
        map[surgeryList()[9].title] = addimageViewList(9)
        map[surgeryList()[10].title] = addimageViewList(10)


        return map
    }

    @Synchronized
    fun orerViewHashMap(): HashMap<String, List<String>> {
        val map = HashMap<String, List<String>>()
        map[surgeryList()[0].title] = addOverViewList(0)
        map[surgeryList()[1].title] = addOverViewList(1)
        map[surgeryList()[2].title] = addOverViewList(2)
        map[surgeryList()[3].title] = addOverViewList(3)
        map[surgeryList()[4].title] = addOverViewList(4)
        map[surgeryList()[5].title] = addOverViewList(5)
        map[surgeryList()[6].title] = addOverViewList(6)
        map[surgeryList()[7].title] = addOverViewList(7)
        map[surgeryList()[8].title] = addOverViewList(8)
        map[surgeryList()[9].title] = addOverViewList(9)
        map[surgeryList()[10].title] = addOverViewList(10)
        return map
    }

    @Synchronized
    fun techHashMap(): HashMap<String, List<String>> {
        val map = HashMap<String, List<String>>()
        map[surgeryList()[0].title] = addtechList(0)
        map[surgeryList()[1].title] = addtechList(1)
        map[surgeryList()[2].title] = addtechList(2)
        map[surgeryList()[3].title] = addtechList(3)
        map[surgeryList()[4].title] = addtechList(4)
        map[surgeryList()[5].title] = addtechList(5)
        map[surgeryList()[6].title] = addtechList(6)
        map[surgeryList()[7].title] = addtechList(7)
        map[surgeryList()[8].title] = addtechList(8)
        map[surgeryList()[9].title] = addtechList(9)
        map[surgeryList()[10].title] = addtechList(10)
        return map
    }

    @Synchronized
    fun tipsHashMap(): HashMap<String, List<String>> {
        val map = HashMap<String, List<String>>()
        map[surgeryList()[0].title] = addtipsList(0)
        map[surgeryList()[1].title] = addtipsList(1)
        map[surgeryList()[2].title] = addtipsList(2)
        map[surgeryList()[3].title] = addtipsList(3)
        map[this.surgeryList()[4].title] = addtipsList(4)
        map[surgeryList()[5].title] = addtipsList(5)
        map[surgeryList()[6].title] = addtipsList(6)
        map[surgeryList()[7].title] = addtipsList(7)
        map[surgeryList()[8].title] = addtipsList(8)
        map[surgeryList()[9].title] = addtipsList(9)
        map[surgeryList()[10].title] = addtipsList(10)
        return map
    }

    /**
     * Overview List
     */
    fun addOverViewList(type: Int): List<String> {
        val arrayList = ArrayList<String>()
        when (type) {
            0 -> {
                arrayList.clear()
                arrayList.add("Performed commonly for total knee arthroplasty and ACL reconstruction")
                arrayList.add("Provides mostly sensory blockade of the anterior knee")
                arrayList.add("Sensory only block may allow for early post operative ambulation")
                return arrayList
            }
            1 -> {
                arrayList.clear()
                arrayList.add("Commonly performed for rib fractures and surgery involving the back and chest including mastectomy and thoracotomy")
                arrayList.add("Comparable to paravertebral block")
                arrayList.add("High volume fascial plane block")
                return arrayList
            }
            2 -> {
                arrayList.clear()
                arrayList.add("Performed commonly for total knee arthroplasty and ACL reconstruction")
                arrayList.add("Provides motor and sensory blockade of the anterior and medial thigh and knee")
                arrayList.add("Motor blockade inhibits early postoperative ambulation")
                return arrayList

            }
            3 -> {
                arrayList.clear()
                arrayList.add("Commonly performed for surgery of the hip and proximal thigh including total hip arthroplasty and ORIF of the hip")
                arrayList.add("Provides motor and sensory blockade of the anterior and lateral thigh")
                arrayList.add("Targets the femoral, lateral femoral cutaneous and obturator nerves")
                arrayList.add("Large volume fascial plane block")
                arrayList.add("Motor blockade may inhibit postoperative ambulation")
                return arrayList
            }
            4 -> {
                arrayList.clear()
                arrayList.add("Performed commonly for shoulder arthroscopy, total shoulder replacement and surgery of the upper arm")
                arrayList.add("Provides both motor and sensory blockade of the shoulder and upper arm")
                arrayList.add("Interscalene block should be avoided in patients diagnosed with pulmonary comorbidities as the phrenic nerve is commonly anesthetized")
                return arrayList
            }
            5 -> {
                arrayList.clear()
                arrayList.add("IPACK stands for Injection between the Popliteal Artery and Capsule of the Knee")
                arrayList.add("Commonly performed for total knee arthroplasty and ACL reconstruction")
                arrayList.add("Provides sensory only blockade to the posterior aspect of the knee")
                return arrayList
            }
            6 -> {
                arrayList.clear()
                arrayList.add("Performed commonly for total knee arthroplasty, ACL reconstruction and surgeries of the foot, ankle and achilles tendon")
                arrayList.add("Provides motor and sensory blockade of the lower leg and foot excluding a variable strip of skin in the medial leg and foot which is innervated by the saphenous nerve")
                arrayList.add("Motor blockade inhibits early postoperative ambulation")
                return arrayList
            }
            7 -> {
                arrayList.clear()
                arrayList.add("Commonly performed for surgery of the abdomen for both laparoscopic and open procedures. Has also been used for hip surgery.")
                arrayList.add("may provide both somatic and visceral analgesia of the abdominal wall from T7-L1")
                arrayList.add("Multiple approaches described in literature including lateral, anterior, posterior and transmuscular")
                arrayList.add("High volume fascial plane block")
                return arrayList
            }
            8 -> {
                arrayList.clear()
                arrayList.add("Performed commonly for surgery involving the umbilical area of the abdomen for both open and laparoscopic surgeries")
                arrayList.add("Provides sensory blockade of the abdominal wall T7-T10")
                arrayList.add("Provides only somatic analgesia of the abdominal wall, does not provide visceral analgesia")
                return arrayList
            }
            9 -> {
                arrayList.clear()
                arrayList.add("Commonly performed for surgery of the elbow, forearm, hand and possibly the shoulder")
                arrayList.add("Provides both a motor and sensory blockade")
                arrayList.add("Onset is very fast")

                return arrayList
            }
            10 -> {
                arrayList.clear()
                arrayList.add("Performed commonly for surgery involving the abdomen for both laparoscopic and open procedures")
                arrayList.add("Provides only somatic analgesia. Does not provide visceral analgesia")
                arrayList.add("High volume fascial plane block")
                return arrayList
            }
        }
        return arrayList
    }


    /**
     * Technique List
     *
     */
    fun addtechList(type: Int): List<String> {
        val arrayList = ArrayList<String>()
        when (type) {
            0 -> {
                //Adductor Canal
                arrayList.clear()
                arrayList.add("Position: Supine, leg slightly externally rotated with knee slightly bent")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 15-25ml unilaterally")
                return arrayList
            }
            1 -> {
                //ESP
                arrayList.clear()
                arrayList.add("Position: Sitting or prone")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 15-25ml")
                return arrayList
            }
            2 -> {
                //Femoral
                arrayList.clear()
                arrayList.add("Position: Supine")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 15-25ml unilaterally")
                return arrayList

            }
            3 -> {
                //FICB
                arrayList.clear()
                arrayList.add("Position: Supine")
                arrayList.add("Probe: 5-10mHz linear or curvilinear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 40-60ml")
                return arrayList
            }
            4 -> {
                //Interscalene
                arrayList.clear()
                arrayList.add("Position: supine or beach chair")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 2-4cm")
                arrayList.add("Approach: In plane posterior approach")
                arrayList.add("LA Volume: 10-25ml")
                return arrayList
            }
            5 -> {
                //IPACK
                arrayList.clear()
                arrayList.add("Position: Supine with knee bent and leg slightly externally rotated")
                arrayList.add("Probe: 5-20mHz linear or 2-5mHz curvilinear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane medial to lateral")
                arrayList.add("LA Volume: 15-25ml")
                return arrayList
            }
            6 -> {
                //Popliteal Sciatic
                arrayList.clear()
                arrayList.add("Position: Supine with leg elevated and straight (recommended); prone or lateral also acceptable")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 10-15ml")
                return arrayList
            }
            7 -> {
                //QL
                arrayList.clear()
                arrayList.add("Position: Supine or Lateral")
                arrayList.add("Probe: 5-10mHz linear or 2-5mHz curvilinear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 15-30ml")
                return arrayList
            }
            8 -> {
                //Rectus
                arrayList.clear()
                arrayList.add("Position: Supine")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 10-15ml each side")
                return arrayList
            }
            9 -> {
                //Supraclavicular
                arrayList.clear()
                arrayList.add("Position: Supine or beach chair")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 2-4cm")
                arrayList.add("Approach: In plane")
                arrayList.add("LA Volume: 15-25ml")

                return arrayList
            }
            10 -> {
                //Tap
                arrayList.clear()
                arrayList.add("Position: Supine")
                arrayList.add("Probe: 5-10mHz linear")
                arrayList.add("Needle: 4-6cm")
                arrayList.add("Approach: In plane medial to lateral/posterior")
                arrayList.add("LA Volume: 20-30ml")
                return arrayList
            }
        }
        return arrayList
    }

    /**
     * Tips List
     */
    fun addtipsList(type: Int): List<String> {
        val arrayList = ArrayList<String>()
        when (type) {
            0 -> {
                //Adductor Canal
                arrayList.clear()
                arrayList.add("Care should be taken to perform the block distal to mid thigh as a more proximal injection may lead to a femoral nerve blockade")
                arrayList.add("Injection both medial and lateral to the femoral artery increases consistency of analgesia")
                arrayList.add("may be combined with IPACK block for more complete coverage of the knee")
                return arrayList
            }
            1 -> {
                //ESP
                arrayList.clear()
                arrayList.add("Commonly performed at the level of T3-4 for breast surgery")
                arrayList.add("Ribs are rounded hyperechoic structures by ultrasound visualization")
                arrayList.add("Transverse processes are square hyperechoic structures by ultrasound visualization")
                return arrayList
            }
            2 -> {
                //Femoral
                arrayList.clear()
                arrayList.add("FNB should consistently be performed at the level of the common femoral artery")
                arrayList.add("Needle visualization is imperative to avoiding intravascular puncture and/or femoral nerve injury")
                arrayList.add("A nerve stimulator may be used in addition to ultrasound guidance to assist in both identifying the nerve and/or avoiding an intraneural injection")
                arrayList.add("IV sedation during the block procedure should still allow for meaningful communication with staff to assist in the detection of Local Anesthetic Toxicity and possible intraneural injection")

                return arrayList

            }
            3 -> {
                //FICB
                arrayList.clear()
                arrayList.add("The sartorious muscle and internal oblique muscle create the “bow tie” seen by ultrasound examination")
                arrayList.add("The needle approach should elicit 2 distinct “pops” while entering fascia lliata and fascia iliacus respectively")
                arrayList.add("Injection should occur deep to the fascia iliaca just superficial to the iliacus muscle")
                return arrayList
            }
            4 -> {
                //Interscalene
                arrayList.clear()
                arrayList.add("Exam should begin at the supraclavicular level. The brachial plexus should be followed to the level of the C5, C6 nerve roots")
                arrayList.add("The “snowman” or “stoplight” of C5, C6 and C7 nerve roots is commonly just the C5 nerve root and a bifurcated C6 nerve root")
                arrayList.add("C7 blockade is not necessary for surgery of the shoulder and upper arm")
                arrayList.add("A shallow posterior in plane approach is used to avoid the long thoracic and supra scapular nerves located in the middle scalene muscle")
                arrayList.add("A nerve stimulator may be used in addition to ultrasound guidance to assist in both identifying the nerve and/or avoiding an intraneural injection")
                arrayList.add("IV sedation during the block procedure should still allow for meaningful communication with staff to assist in the detection of Local Anesthetic Toxicity and possible intraneural injection")
                return arrayList
            }
            5 -> {
                //IPACK
                arrayList.clear()
                arrayList.add("Targets the genicular innervation to the knee")
                arrayList.add("Injection should occur deep to the popliteal artery")
                arrayList.add("Needle visualization is imperative to avoiding intravascular injection")

                return arrayList
            }
            6 -> {
                //Popliteal Sciatic
                arrayList.clear()
                arrayList.add("Exam should begin within the posterior fossa of the knee superficial to popliteal artery. The tibial nerve will be located superficial to the popliteal artery, the smaller peroneal nerve will be located superficial and lateral to the tibial nerve.")
                arrayList.add("Very little local anesthetic is needed to provide a profound sciatic nerve block")
                arrayList.add("A nerve stimulator may be used in addition to ultrasound guidance to assist in both identifying the nerve and/or avoiding an intraneural injection")
                arrayList.add("IV sedation during the block procedure should still allow for meaningful communication with staff to assist in the detection of Local Anesthetic Toxicity and possible intraneural injection")
                return arrayList
            }
            7 -> {
                //QL
                arrayList.clear()
                arrayList.add("IV sedation during the block procedure should still allow for meaningful communication with staff to assist in the detection of Local Anesthetic Toxicity and possible intraneural injection")
                arrayList.add("LA is deposited deep to the the aponeurosis of the transverse abdominis muscle near the ql muscle for the lateral ql block")
                arrayList.add("Literature suggests the target for the QL block is the thoracolumbar fascia surrounding the QL muscle and potentially the paravertebral space itself.")
                arrayList.add("The thoracolumbar fascia contains a network of sympathetic fibers and mechanoreceptors")
                return arrayList
            }
            8 -> {
                //Rectus
                arrayList.clear()
                arrayList.add("Similar to a fascial plane block, the block is somewhat volume dependent")
                arrayList.add("A medial to lateral approach is recommended to help avoid epigastric arteries")
                arrayList.add("Injection to occur superficial to transversalis fascia and deep to rectus muscle")
                arrayList.add("Ultrasound needle visualization is imperative to avoiding peritoneum")
                return arrayList
            }
            9 -> {
                //Supraclavicular
                arrayList.clear()
                arrayList.add("Needle visualization is imperative to avoiding intravascular puncture, brachial plexus injury and pneumothorax")
                arrayList.add("Multiple injection sites around brachial plexus may ensure complete coverage of brachial plexus")
                arrayList.add("A nerve stimulator may be used in addition to ultrasound guidance to assist in both identifying the nerve and avoiding intraneural injection")

                return arrayList
            }
            10 -> {
                //Tap
                arrayList.clear()
                arrayList.add("Needle visualization imperative to avoiding peritoneal puncture")
                arrayList.add("may be combined with rectus sheath block for more complete analgesia of the abdominal wall")
                arrayList.add("Care should be taken to avoid needle insertion at potential laparoscopic port sites")
                arrayList.add("Approach: In plane medial to lateral/posterior")
                arrayList.add("A more lateral/posterior injection of LA will allow for optimal coverage of low abdominal section while a more medial/anterior injection will allow for better coverage of mid/upper abdomen")
                return arrayList
            }
        }
        return arrayList
    }

    val ABOUTUS_HTML = "file:///android_asset/aboutus.html"


    val ABDOMINAL_SURGARY_CONTENT = "file:///android_asset/eri_abdominal_surgery.html"


    val HIP_AURTHO_CONTENT = "file:///android_asset/eri_total_hip_arthroplasty.html"


    val KNEE_ARTHO_CONTENT = "file:///android_asset/eri_total_knee_arthroplasty.html"

    val TERMS_CONTENT = "file:///android_asset/terms_of_use.html"

    val MULTIMODAL_MEDS_CONTENT = "file:///android_asset/multimodelmed.html"

    val MULTIMEDIA_TEXT = "<!DOCTYPE html>\n" +
            "<html>\n" +
            "<head>\n" +
            "<style>\n" +
            "@import url('https://fonts.googleapis.com/css2?family=DM+Sans&display=swap');\n" +
            "</style>\n" +
            "\n" +
            "<style>\n" +
            ".p{\n" +
            "    font-family: DM sans-serif;\n" +
            "}\n" +
            "</style>\n" +
            "\n" +
            "</head>\n" +
            "<body>\n" +
            "\n" +
            "<p><strong>Celebrex</strong></p>\n" +
            "<p><p>●200-400mg PO</p>\n" +
            "<p><p>●Contraindicated in renal failure patients,\n" +
            "thrombocytopenia, hemorrhage, patients at risk of\n" +
            "postoperative bleeding, CABG patients with hx of\n" +
            "thromboembolic events</p>\n" +
            "\n" +
            "<p><strong>Clonidine</strong></p>\n" +
            "<p>●2-5mcg/kg IV</p>\n" +
            "<p>●0.2-0.5mcg/kg/hr infusion</p>\n" +
            "<p>●5-7mcg/kg PO</p>\n" +
            "<p>●may cause bradycardia and hypotension</p>\n" +
            "\n" +
            "<p><strong>Decadron</strong></p></p>\n" +
            "<p>●4-12mg IV</p>\n" +
            "<p>●may elevate blood glucose levels, avoid in bowel\n" +
            "resection sx</p>\n" +
            "\n" +
            "<p><strong>Dexmedetomidine</strong></p>\n" +
            "<p>1mcg/kg bolus given over 10 minutes</p>\n" +
            "<p>0.2-1 mcg/kg/min infusion</p>\n" +
            "<p>Alternatively may be dosed in 4-8mcg boluses</p>\n" +
            "<p>may cause bradycardia, hyper/hypotension</p>\n" +
            "\n" +
            "<p><strong>Esmolol</strong></p>\n" +
            "<p>●0.5-1mg/kg bolus</p>\n" +
            "<p>●5-500mcg/kg/min infusion</p>\n" +
            "<p>●Avoid in patients with AV block or bradycardia</p>\n" +
            "\n" +
            "<p><strong>Gabapentin</strong></p>\n" +
            "<p>300-1200mg PO</p>\n" +
            "<p>Avoid higher doses as postoperative sedation may occur</p>\n" +
            "\n" +
            "<p><strong>Ketamine</strong></p>\n" +
            "<p>●0.2-0.5mg/kg bolus (25-50mg)</p>\n" +
            "<p>●2-10 mcg/kg/min infusion</p>\n" +
            "<p>●Avoid in elderly patients</p>\n" +
            "\n" +
            "<p><strong>Lidocaine</strong></p>\n" +
            "<p>●1.5mg/kg bolus</p>\n" +
            "<p>●2-3mg/kg/hr infusion intraoperatively</p>\n" +
            "<p>●1-1.3mg/kg/hr infusion postoperatively</p>\n" +
            "<p>●Consider total dose of LA if blocks performed</p>\n" +
            "\n" +
            "<p><strong>Magnesium Sulfate</strong></p>\n" +
            "<p>●30-50mg/kg bolus</p>\n" +
            "<p>●10mg/kg/hr infusion</p>\n" +
            "<p>●may inhibit Acetylcholine release and prolongs</p>\n" +
            "\n" +
            "<p><strong>neuromuscular blockade</strong></p>\n" +
            "<p>●Avoid in renal failure patients</p>\n" +
            "\n" +
            "<p><strong>Metoprolol</strong></p>\n" +
            "<p>●Sympathetic modulator</p>\n" +
            "<p>●1-5mg IV</p>\n" +
            "<p>●Long half life</p>\n" +
            "<p>●Avoid in patients with AV block and bradycardia</p>\n" +
            "\n" +
            "<p><strong>Nitrous Oxide</strong></p>\n" +
            "<p>●Limit to 50%</p>\n" +
            "\n" +
            "<p><strong>Pregabalin</strong></p>\n" +
            "<p>●75-150mg PO</p>\n" +
            "<p>●may cause increased PR interval, angioedema,\n" +
            "thrombocytopenia and rhabdomyolysis</p>\n" +
            "\n" +
            "<p><strong>Toradol</strong></p>\n" +
            "<p>●15-30mg IV</p>\n" +
            "<p>●Contraindicated in renal failure patients,\n" +
            "thrombocytopenia, hemorrhage, patients at risk of\n" +
            "postoperative bleeding, CABG patients with hx of\n" +
            "thromboembolic events</p>\n" +
            "\n" +
            "</body>\n" +
            "</html>\n"


    /**
     * Text
     */
    var ADDUCTOR_ANATOMY_TEXT =
        "The adductor canal is formed by the fascial planes of 3 different muscles. The anterior aspect of the adductor canal is formed by the sartorius muscle. " +
                "The sartorius muscle is often described as a boat shaped structure on ultrasound. The adductor canal may also be described as being sub-sartorial. In addition to the sartorius muscle, the adductor canal is bordered by the vastus medialis and the adductor longus muscles. Beginning at the proximal end of the adductor canal and  scanning distally, the femoral artery , also found in the adductor canal, can be seen on ultrasound \"moving\" medial to lateral along the posterior border of the sartorius muscle before diving deep to the sartorius in the distal thigh. In the mid thigh the saphenous nerve is most often located lateral to the femoral artery in the adductor canal between the sartorius and vastus medialis muscles although branches of the saphenous nerve do occasionally occur medial to the femoral artery as well. The saphenous nerve at the level of the adductor canal can be blocked for procedures involving the distal thigh, femur, knee and the medial aspect of the lower leg. The adductor canal block is often performed with or without a sciatic nerve block or ipack block for ACL reconstruction and total knee arthroplasty."

    var ADDUCTOR_BLOCK_TEXT =
        "The adductor canal block should be performed near the mid thigh level. The goal is to place 10-20ml of local anesthetic lateral to the femoral artery in the adductor canal defined as the space lateral to the femoral artery between the sartorius and the vastus medialis muscles. The medial side of the femoral artery may be accessed by hydrodissection superficial to the femoral artery. Approximately 5-10ml of local anesthetic is placed medial to the femoral artery for a more complete coverage of the saphenous nerve. Local anesthetic spread around the artery should be confirmed by ultrasound visualization."

    var ESP_ANATOMY_TEXT =
        "The erector spinae is a group of muscles and tendons which run the length of the spine just lateral to the vertebral column bilaterally. At the T5 level (mid scapula) the erector spinae is immediately superficial to the transverse process of the spine. The rhomboid and trapezius muscles are found superficial to the erector spinae muscle at the same level. The paravertebral space and pleura is located between and deep to the transverse processes. Analgesia for the ESP block occurs form the diffusion of local anesthesia anteriorly to the ventral and dorsal rami of the spinal nerves."
    var ESP_PROCEDURE_TEXT =
        "The erector spinae plane block is performed with the patient in the sitting position. The ultrasound probe is placed in the cranial caudal orientation. The transverse process should be identified at the T5 level (mid scapula). Lateral to the transverse process the ribs may be identified as rounded hyperechoic structures by ultrasound. The rib may be followed medially giving rise to the transverse process which is identified as a square hyperechoic structure. The erector spinae, rhomboid and trapezius muscles are identified superficial to the transverse process respectively. The block needle is inserted  using the in plane cranial to caudal approach. The transverse process may be used as a target for the block needle to minimize the chance of entering the paravertebral space which would increase chances for a pneumothorax. After the block needle meets the transverse process the needle may be withdrawn slightly to allow injection of 20-40ml of local anesthetic just deep to the erector spinae.. The local anesthetic will spread in the cranial caudal directions allowing for coverage of multiple dermatomes of the back and chest."

    var FORMORAL_ANATOMY_TEXT =
        "The ultrasound guided femoral nerve block is performed at the level of the inguinal crease. The femoral nerve is identified just lateral to the femoral artery and deep to the fascia iliaca. The femoral nerve is hyper echoic and oval in shape. It should be noted the femoral vein is medial to the femoral artery. The block should be performed consistently at the level of the common femoral artery. If the patient is obese the panis may be taped to help expose the inguinal crease."
    var FORMORAL_PROCEDURE_TEXT =
        "The ultrasound guided femoral nerve block is performed at the level of the inguinal crease. The femoral nerve is identified just lateral to the femoral artery and deep to the fascia iliaca. The femoral nerve is hyper echoic and oval in shape. It should be noted the femoral vein is medial to the femoral artery. The block should be performed consistently at the level of the common femoral artery. If the patient is obese the panis may be taped to help expose the inguinal crease."

    var FICB_ANATOMY_TEXT =
        "The fascia iliaca compartment block is a varied approach to the lumbar plexus block. A large volume of local anesthetic is injected deep to the fascia iliaca about one third of the way between the anterior superior iliac spine and the pubic symphysis. This anatomical plane deep to the fascia iliaca contains at different points the femoral, lateral femoral cutaneous and obturator nerves. These nerves arise from the lumbar plexus T12-L5 and provide most of the innervation to the hip and proximal femur."
    var FICB_PROCEDURE_TEXT =
        "The panis should be taped out of the way if necessary. The ultrasound probe is placed perpendicular to the floor over the anterior superior iliac spine. The probe is then rotated approximately 20 degrees as it is slid medially 1/3rd of the distance to the pubic symphysis. Care should be taken to stay lateral to the iliac artery. Scan to identify the \"bow tie\" structure which consists of the sartorius muscle, internal oblique muscle, fascia lata, fascia iliaca and iliacus muscle. PNBschool recommends an in plane approach with the needle inserted inferiorly to the ultrasound probe. Two \"pops” will be felt as the needle passes through the fascia lata and the fascia iliaca. The needle should be advanced just beneath the fascia iliaca and 40-60ml of local anesthetic should be injected. Cephalad spread of local anesthetic should be observed. The block needle should be visualized at all times to assure the abdominal cavity is not punctured."

    var INTERSCALEN_ANATOMY_TEXT =
        "The brachial plexus at the interscalene level is located just lateral to the carotid artery within the interscalene groove consisting of the anterior and middle scalene muscles. This block is performed at the level of the cricoid cartilage but is more importantly performed at the level where the brachial plexus is optimally visualized by ultrasound. This may be patient dependent. The brachial plexus  at the level of the cricoid cartilage is described as being oriented in a vertical arrangement where the C5, C6 and C7 nerve roots are visualized by ultrasound."
    var INTERSCALEN_PROCEDURE_TEXT =
        "The patient is positioned in the supine position slightly head up. The ultrasound probe is placed cephalad to the the clavicle and the subclavian artery is identified. The brachial plexus will be identified just lateral to the subclavian artery. The ultrasound probe is then moved superiorly following the brachial plexus to the level of the cricoid cartilage. The C5, C6 and C7 nerve roots will be easily identified by ultrasound. The block needle is inserted posteriorly using the in plane approach. The block needle should be visualized by ultrasound at all times. A nerve stimulator may be used to help identify the brachial plexus. As the needle approaches the brachial plexus a motor response from the biceps muscle or muscle groups below the elbow should be elicited. Local anesthetic, 10-20 ml, is injected and distribution should be visualized around the brachial plexus. The needle may be moved while visualized by ultrasound. to ensure even spread of the local anesthetic. It should be noted that the local anesthetic should be easy to inject without paresthesia. Epinephrine may be added to the local anesthetic to ensure an intravascular injection has not occurred."

    var IPACK_ANATOMY_TEXT =
        "The iPACK block stands for Injection between the Popliteal Artery and posterior Compartment of the Knee '' and provides a sensory block specifically to the posterior aspect of the knee. The anterior aspect of the knee is commonly covered by an entirely different nerve block such as the adductor canal block or femoral nerve block. The iPACK block targets the posterior genicular innervation of the knee. The posterior genicular nerves consist of  articular branches of the common peroneal, tibial and obturator nerves. These nerves provide mostly sensory innervation to the knee posteriorly. The absence of significant motor block is the reason for choosing the iPACK over the popliteal sciatic nerve block. Simply stated, patients can undergo physical therapy sooner with an iPACK block. As seen in image 1 the medial and lateral head of the gastrocnemius muscle, the sartorious muscle, biceps muscle along with the femoral condyles may all be visualized during the block."
    var IPACK_PROCEDURE_TEXT =
        "The ultrasound guided iPACK block is performed with the patient positioned supine, hip abducted and the knee flexed. The ultrasound probe is positioned posteriorly slightly proximal to the flexion line of the knee. A curvilinear or linear probe may be used to perform the block using  an in plane technique. The needle is inserted medial and directed to the space between the popliteal artery and lateral condyle of the knee. If using a linear probe it will need to be moved in order to visualize the needle as it is advanced to the target in a medial to lateral approach while a curvilinear probe may visualize all necessary structures including the approach of the block needle and spread of local anesthetic without being repositioned. Ultrasound visualization during injection should show spread of local anesthetic from the lateral to medial condyle between the popliteal artery and condyles. A potential genicular space has been described which when entered with the block needle allows for adequate spread of local anesthetic without repositioning."

    var QL_ANATOMY_TEXT =
        "The ultrasound guided quadratus lumborum block has recently evolved as a solution for postoperative analgesia for major abdominal surgery. The ultrasound guided QL block provides both somatic and visceral analgesia therefore in many cases seems superior to the lateral TAP block which provides only somatic analgesia. The QL block may provide analgesia ranging from the T7-L1 dermatomes. Four approaches to the ultrasound guided QL approach have been described in recent literature. These are the QL1 or lateral approach, QL2 or posterior approach, QL3 or anterior approach and QL4 the intramuscular approach. The most common and in our opinion the easiest approach to learn is the QL1 approach thus this tutorial will concentrate on explaining only the QL1 approach. The goal of performing a QL1 block is to deposit local anesthetic deep to the aponeurosis of the transverse abdominis muscle but superficial to the transversalis fascia. This location is slightly medial to quadratus lumborum. The local anesthetic injected in this fascial plane communicates (spreads) with the thoracolumbar fascial plane and eventually the paravertebral space by dissecting the space around the QL muscle. The thoracolumbar fascia itself contains a network of sympathetic fibers in addition to mechanoreceptors."
    var QL_PROCEDURE_TEXT =
        "The ultrasound guided QL1 block may be performed with the patient in the supine position. It should be noted that medial retraction of the abdomen may facilitate this approach in the obese patient. A linear transducer is placed on the midaxillary line between the iliac crest and costal margins. The external oblique, internal oblique and transversus abdominis muscles should be easily identified. The transducer is then moved posterior until the aponeurosis of the transverse abdominis muscle is identified. The block needle is inserted in plane anterior to the transducer. The target for deposition of local anesthetic is deep to the TAM aponeurosis (thoracolumbar fascia) medial to the QL muscle. Similar to most fascial plane blocks the QL block is a volume block thus 20-30ml of local anesthetic is used. The concentration of local anesthetic used may be adjusted taking into consideration the patients weight and whether performing a unilateral vs. bilateral injection to avoid a toxic dose."

    var RECTUS_ANATOMY_TEXT =
        "The rectus muscles are located in the upper abdomen just lateral to the lines alba, bilaterally. The rectus sheath, also called the rectus fascia, is formed by the aponeurosis of the internal and external oblique muscles and the transverse abdominal muscle. The rectus sheath is divided into the anterior and posterior laminae. The peritoneum and transversalis fascia is located deep to the rectus muscles. The 9th, 10th and 11th intercostal nerves are located between the rectus muscle and the posterior rectus sheath."
    var RECTUS_PROCEDURE_TEXT =
        "The rectus sheath block is performed in the supine position. The ultrasound probe may be placed subcostal in a transverse orientation just lateral to the linea alba. The block needle is inserted from the medial to lateral orientation using in plane technique. The block needle will pass through the anterior rectus sheath and continue through the rectus muscle until it rests just superficial to the transversalis fascia plane and deep to the rectus muscle. 10-15ml of local anesthetic is injected. The spread of local anesthetic should be seen lateral and medial just deep to the rectus muscle and superficial to the transversalis fascia.    "

    var SUPRACLAVICULAR_ANATOMY_TEXT =
        "The brachial plexus is located lateral and superficial to the subclavian artery at the supraclavicular level and is identified as a cluster of hypoechoic round structures often described as a bunch of grapes. The subclavian artery is located at the midpoint of the clavicle. The first rib is identified deep to the subclavian artery and appears as a hyperechoic mostly linear structure by ultrasound exam. The parietal pleura may be identified lateral and medial to the first rib."
    var SUPRACLAVICULAR_PROCEDURE_TEXT =
        "The supraclavicular block is performed with the patient supine and back slightly elevated. The head is turned away from the block site to allow the ultrasound to be manipulated supraclavicularly. The ultrasound probe is positioned above the clavicle in a parallel orientation to the clavicle. The subclalvian artery is identified near the midpoint of the clavicle by ultrasound. The brachial plexus will be identified lateral and superficial to the subclavian artery. The block needle is inserted laterally to the ultrasound probe using the enplane approach. Care should be taken to ensure the block needle remains superficial to the first rib and parietal pleura. This is accomplished by maintaining constant visualization of the block needle throughout the procedure. Local anesthetic is delivered lateral and superficial to the brachial plexus. Adequate spread of local anesthetic should be visualized by ultrasound guidance. Local anesthetic is injected only after careful aspiration to avoid an inadvertent intravascular injection."

    var TAP_ANATOMY_TEXT =
        "The anterior rami of the lower six thoracic nerves T7-T12 and the first lumbar nerve, L1, innervate the anterior abdominal wall. Terminal branches of these nerves travel to the abdominal wall through a plane between the internal oblique and transverse abdominal muscles. The plane between these muscles is named the transverse abdominal plane. Ultrasound imaging of the abdominal wall at the anterior axillary line between the iliac crest and costal margin reveals 3 muscles. These muscles are the external oblique, internal oblique and transverse abdominal muscles. The muscles are separated by fascia which is hyperechoic on ultrasound. The target nerves for the TAP block are located between the transverse abdominal muscle and the fascia above the muscle. These nerves are commonly not visualized during ultrasound exams. The transversalis fascia lies just beneath the transverse abdominal muscle followed by the peritoneum containing the bowels which can be identified by peristalsis."
    var TAP_PROCEDURE_TEXT =
        "The TAP block is performed with the patient in the supine position. The ultrasound probe is placed between the iliac crest and costal margin in the mid axillary line. The external oblique, internal oblique and transverse abdominal muscles are identified during the initial ultrasound exam. The transverse abdominal muscle is the thinnest of the muscles and located just superficial to the peritoneum. The block needle is inserted 1-3cm medial to the transducer using the in plane approach. A \"pop\" may be felt as the needle is inserted just beneath the fascial layer separating the internal oblique muscle and the transverse abdominal muscle. This plane is named the transverse abdominal plane. After the needle is carefully aspirated 20-30ml of local anesthetic is injected. Proper needle placement will show the deposition and spread of local anesthetic between the transverse abdominal muscle and the fascia above. The transverse abdominal muscle will be pushed deep to the fascia above as the local anesthetic volume injected is increased. This will help confirm proper placement of the block needle. The block needle should be visualized by ultrasound during the block."

    var POP_ANATOMY_TEXT =
        "The exam begins with the ultrasound transducer in the transverse orientation at the popliteal crease. The patient may be prone but PNBschool recommends the patient remain supine with the operative leg raised and rested on a table or leg holding device. The popliteal artery is identified in the popliteal fossa. Remember, nerves below the belt line are hyperechoic on ultrasound. The tibial nerve is identified superficial and lateral to the artery. The peroneal nerve is just lateral and superficial to the tibial nerve. Once the tibial nerve is identified on ultrasound the probe is moved proximal while following the tibial nerve as it combines with the peroneal nerve to form the sciatic nerve. The exam depth will need to be adjusted as the probe is moved proximally. Once the sciatic nerve is identified the needle is inserted from the lateral aspect of the leg using the in plane approach. Approximately 10 to 20ml of local anesthetic is injected adjacent to the sciatic nerve with visualization of your block needle at all times. The needle may be moved while visualized to ensure adequate spread of local anesthetic around the sciatic nerve.  The popliteal sciatic nerve block may be used for procedures involving the knee, lower leg and foot excluding a variable strip of skin on the medial leg and foot which is innervated by the saphenous nerve."


    fun AnatomyHashmap(type: String): HashMap<String, String> {
        val anatomyMap = HashMap<String, String>()
        when (type) {
            "Adductor Canal" -> {
                anatomyMap.clear()
                anatomyMap["Adductor Block-Anatomy"] = ADDUCTOR_ANATOMY_TEXT
            }
            "ESP" -> {
                anatomyMap.clear()
                anatomyMap["ESP Block-Anatomy"] = ESP_ANATOMY_TEXT
            }
            "Femoral" -> {
                anatomyMap.clear()
                anatomyMap["Femoral Block-Anatomy"] = FORMORAL_ANATOMY_TEXT
            }
            "FICB" -> {
                anatomyMap.clear()
                anatomyMap["FICB Block-Anatomy"] = FICB_ANATOMY_TEXT
            }
            "Interscalene" -> {
                anatomyMap.clear()
                anatomyMap["Interscalene Block-Anatomy"] = INTERSCALEN_ANATOMY_TEXT
            }
            "IPACK" -> {
                anatomyMap.clear()
                anatomyMap["IPACK Block-Anatomy"] = IPACK_ANATOMY_TEXT
            }
            "Popliteal Sciatic" -> {
                anatomyMap.clear()
                anatomyMap["Popliteal Block-Anatomy"] = POP_ANATOMY_TEXT
            }
            "QL" -> {
                anatomyMap.clear()
                anatomyMap["QL Block-Anatomy"] = QL_ANATOMY_TEXT
            }
            "Rectus" -> {
                anatomyMap.clear()
                anatomyMap["Rectus Block-Anatomy"] = RECTUS_ANATOMY_TEXT
            }
            "Supraclavicular" -> {
                anatomyMap.clear()
                anatomyMap["Supraclavicular Block-Anatomy"] = SUPRACLAVICULAR_ANATOMY_TEXT
            }
            "Tap" -> {
                anatomyMap.clear()
                anatomyMap["Tap Block-Anatomy"] = TAP_ANATOMY_TEXT
            }
        }
        return anatomyMap
    }


    fun procedureHashmap(type: String): HashMap<String, String> {
        val procedureMap = HashMap<String, String>()
        when (type) {
            "Adductor Canal" -> {
                procedureMap.clear()
                procedureMap["Adductor Block-Procedure"] = ADDUCTOR_BLOCK_TEXT
            }
            "ESP" -> {
                procedureMap.clear()
                procedureMap["ESP Block-Procedure"] = ESP_PROCEDURE_TEXT
            }
            "Femoral" -> {
                procedureMap.clear()
                procedureMap["Femoral Block-Procedure"] = FORMORAL_PROCEDURE_TEXT
            }
            "FICB" -> {
                procedureMap.clear()
                procedureMap["FICB Block-Procedure"] = FICB_PROCEDURE_TEXT
            }
            "Interscalene" -> {
                procedureMap.clear()
                procedureMap["Interscalene Block-Procedure"] = INTERSCALEN_PROCEDURE_TEXT
            }
            "IPACK" -> {
                procedureMap.clear()
                procedureMap["IPACK Block-Procedure"] = IPACK_PROCEDURE_TEXT
            }
            "QL" -> {
                procedureMap.clear()
                procedureMap["QL Block-Procedure"] = QL_PROCEDURE_TEXT
            }
            "Rectus" -> {
                procedureMap.clear()
                procedureMap["Rectus Block-Procedure"] = RECTUS_PROCEDURE_TEXT
            }
            "Supraclavicular" -> {
                procedureMap.clear()
                procedureMap["Supraclavicular Block-Procedure"] = SUPRACLAVICULAR_PROCEDURE_TEXT
            }
            "Tap" -> {
                procedureMap.clear()
                procedureMap["Tap Block-Procedure"] = TAP_PROCEDURE_TEXT
            }
        }
        return procedureMap
    }

    fun surgeryList(): List<SurgeryDetails> {
        val surgaryData = ArrayList<SurgeryDetails>()
        surgaryData.add(
            SurgeryDetails(
                R.drawable.aductor,
                "Adductor Canal",
//                "Adductor Canal Block"
                "Adductor Canal"
            )
        )//0
        surgaryData.add(SurgeryDetails(R.drawable.esp_block, "ESP", "ESP Block"))//1
//        surgaryData.add(SurgeryDetails(R.drawable.femoral, "Femoral", "Femoral Nerve Block"))//2
        surgaryData.add(SurgeryDetails(R.drawable.femoral, "Femoral", "Femoral"))//2
        surgaryData.add(SurgeryDetails(R.drawable.ficb, "FICB", "FICB"))//3
        surgaryData.add(
            SurgeryDetails(
                R.drawable.interscalene,
                "Interscalene",
//                "Interscalene Block",
                "Interscalene"
            )
        )//4
        surgaryData.add(
            SurgeryDetails(
                R.drawable.ipack, "IPACK",
//            "IPACK Block"
                "IPACK"
            )
        )//5
        surgaryData.add(
            SurgeryDetails(
                R.drawable.popliteal_sciatic,
                "Popliteal Sciatic",
//                "Popliteal Sciatic Block"
                "Popliteal Sciatic"
            )
        )//6
        surgaryData.add(
            SurgeryDetails(
                R.drawable.ql, "QL",
//            "QL Block"
                "QL"
            )
        )//7
        surgaryData.add(
            SurgeryDetails(
                R.drawable.rectus, "Rectus",
//            "Rectus Sheath Block"
                "Rectus"
            )
        )//8
        surgaryData.add(
            SurgeryDetails(
                R.drawable.supraclavicular,
                "Supraclavicular",
//                "Supraclavicular Block"
                "Supraclavicular"
            )
        )//9
        surgaryData.add(
            SurgeryDetails(
                R.drawable.tap, "Tap",
                "Tap Block"

            )
        )//10
        surgaryData.add(
            SurgeryDetails(
                R.drawable.multimodel,
                "Multimodel",
                "Multimodal Med"
            )
        )//11
        return surgaryData
    }

    /**
     * Overview List
     */
    fun addimageViewList(type: Int): List<Int> {
        val arrayList = ArrayList<Int>()
        when (type) {
            0 -> {
                arrayList.clear()
                arrayList.add(R.drawable.adductor1)
                arrayList.add(R.drawable.aductor2)
                return arrayList
            }
            1 -> {
                arrayList.clear()
                arrayList.add(R.drawable.esp_approach)
                arrayList.add(R.drawable.talisesplabeled)
                return arrayList
            }
            2 -> {
                arrayList.clear()
                arrayList.add(R.drawable.femoral11)
                arrayList.add(R.drawable.femoralblockanatomy)
                return arrayList
            }
            3 -> {
                arrayList.clear()
                arrayList.add(R.drawable.fascialiaca7)
                arrayList.add(R.drawable.ficblabeled)
                return arrayList
            }
            4 -> {
                arrayList.clear()
                arrayList.add(R.drawable.interscaleneapproach)
                arrayList.add(R.drawable.isblabeled)
                return arrayList
            }
            5 -> {
                arrayList.clear()
                arrayList.add(R.drawable.ipackapproach)
                arrayList.add(R.drawable.ipacklabeled)

                return arrayList
            }
            6 -> {
                arrayList.clear()
                arrayList.add(R.drawable.sciaticapproach)
                arrayList.add(R.drawable.popliteal_internal)
                return arrayList
            }
            7 -> {
                arrayList.clear()
                arrayList.add(R.drawable.anteriorqlapproach)
                arrayList.add(R.drawable.lateral_ql_image)
                arrayList.add(R.drawable.posteriorqlapproach)
                arrayList.add(R.drawable.post_ql_image)
//                arrayList.add(R.drawable.lateral_ql_image)
//                arrayList.add(R.drawable.posteriorqlapproach)
//                arrayList.add(R.drawable.post_ql_image)
                return arrayList
            }
            8 -> {
                arrayList.clear()
                arrayList.add(R.drawable.rectus2)
                arrayList.add(R.drawable.rectus1)
                return arrayList
            }
            9 -> {
                arrayList.clear()
                arrayList.add(R.drawable.supraclavicularapproach)
                arrayList.add(R.drawable.supraclavicularimage)
                return arrayList
            }
            10 -> {
                arrayList.clear()
                arrayList.add(R.drawable.tap3)
                arrayList.add(R.drawable.tap2)
                return arrayList
            }
        }
        return arrayList
    }
}


